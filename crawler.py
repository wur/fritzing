# Retrieves all topics and gets all posts for the parts file
from genericpath import isfile
import os
import requests
import json
import time
import zipfile
import xmltodict
import xml.etree.ElementTree as ET
from mdutils.mdutils import MdUtils

def get_all_parts():
    files = []
    dirlist = ['./parts']
    while len(dirlist) > 0:
        for (dirpath, dirnames, filenames) in os.walk(dirlist.pop()):
            dirlist.extend(dirnames)
            files.extend(filenames)
    fzpz = set()
    
    for file in files:
        if file.endswith(".fzpz"):
            fzpz.add(file)
    return fzpz
            
def main():
    root = "https://forum.fritzing.org/latest.json?page="
    # root = "https://forum.fritzing.org/c/parts-submit.json?page="
    # root = "https://forum.fritzing.org/c/parts-help.json?page="
    if not os.path.isdir("json"):
        os.mkdir("json")
    if not os.path.isdir("parts"):
        os.mkdir("parts")
        os.mkdir("parts/other")
    if not os.path.isdir("topic"):
        os.mkdir("topic")
    
    # Get all parts
    fzpz = get_all_parts()
    # Get rid of the range... and check for date later as well
    index = 0
    while True:
        # Keep the sleep, otherwise API block
        time.sleep(1)
        # Next page
        index = index + 1
        # The URL for forum info retrieval
        json_url = root + str(index)
        print("Processing", json_url)
        # Get the content
        r = requests.get(json_url)
        json_content = r.content
        # Turn into a dictionary
        data = json.loads(json_content)
        # If there are no new topics we can stop with the range check
        if len(data['topic_list']['topics']) == 0:
            print(data)
            break
        # For each topic download the json file
        for topic in data['topic_list']['topics']:
            topic_json_file = "topic/"+ topic['slug'] + "-" + str(topic['id']) + ".json"
            # If the file does not exists... TODO, time stamp check
            if not os.path.isfile(topic_json_file):
                r = requests.get("https://forum.fritzing.org/t/" + topic['slug'] + "/" + str(topic['id']) + ".json")
                print("Retrieving " + topic['slug'] + "-" + str(topic['id']))
                with open("topic/"+ topic['slug'] + "-" + str(topic['id']) + ".json",'wb') as f:
                    f.write(r.content)
            # Process the post and download .fzpz files when present
            
            with open(topic_json_file) as json_file:
                json_content = json_file.read()
                # If slow down, remove file
                if json_content.startswith('Slow down'):
                    print("Removing", topic_json_file)
                    os.remove(topic_json_file)
                    time.sleep(10)
                    continue
                data = json.loads(json_content)
                for post in data['post_stream']['posts']:
                    if ".fzpz" in post['cooked']:
                        content = post['cooked'].split()
                        for element in content:
                            if not element.startswith("href=\"/uploads"): continue
                            element = element.replace("</a>", "")
                            url = "http://forum.fritzing.org/" + element.split("\"")[1]
                            filename = element.split(">")[-1].replace(".fzpz","") + "_" + element.split("\"")[1].split("/")[-1]
                            if not filename.endswith("fzpz"): continue
                            if filename in fzpz: continue
                            r = requests.get(url)
                            with open("parts/other/" + filename,'wb') as f:
                                f.write(r.content)
                            time.sleep(1)
                    elif ".fzpz" in json.dumps(post):
                        pass # print("!!!", post)

if __name__ == '__main__':
    main()
    # fzpz()
    
