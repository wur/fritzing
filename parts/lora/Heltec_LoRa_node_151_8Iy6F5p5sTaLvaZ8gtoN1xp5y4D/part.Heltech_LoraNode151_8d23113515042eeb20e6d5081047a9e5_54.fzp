<?xml version='1.0' encoding='UTF-8'?>
<module moduleId="Heltech_LoraNode151_8d23113515042eeb20e6d5081047a9e5_54" referenceFile="generic_ic_dip_28_300mil.fzp">
 <version>4</version>
 <author>A.Bence</author>
 <title>Heltec_LoRa_node_151</title>
 <label>Lora_node151</label>
 <date>mer. déc. 23 2020</date>
 <tags>
  <tag>lora</tag>
  <tag>LowPower</tag>
  <tag>Heltec</tag>
  <tag>STM32L</tag>
  <tag>SX1276</tag>
  <tag>semtech</tag>
 </tags>
 <properties>
  <property name="family">Generic IC</property>
  <property name="variant">variant 2</property>
  <property name="pins">36</property>
  <property name="chip label">LoRaNode151</property>
 </properties>
 <taxonomy>part.dip.28.pins</taxonomy>
 <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
&lt;html>&lt;head>&lt;meta name="qrichtext" content="1" />&lt;style type="text/css">
p, li { white-space: pre-wrap; }
&lt;/style>&lt;/head>&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:7.8pt; font-weight:400; font-style:normal;">
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">Based on ultra low power design, we launched LoRa Node 151， adopting the &lt;a href="https://www.st.com/content/st_com/en/products/microcontrollers-microprocessors/stm32-32-bit-arm-cortex-mcus/stm32-ultra-low-power-mcus/stm32l1-series/stm32l151-152/stm32l151cc.html">&lt;span style=" text-decoration: underline; color:#0000ff;">STM32L151CCU6&lt;/span>&lt;/a> MCU (support DFU mode) and SX127x LoRa chip, use &lt;span style=" font-weight:600;">1/2 AA Lithium chlorine sulfite battery&lt;/span>. Enter low power mode under the standard LoRaWAN protocol, &lt;span style=" font-weight:600;">the Deep Sleep Current ≤ 1.8uA&lt;/span>.&lt;/p>&lt;/body>&lt;/html></description>
 <views>
  <iconView>
   <layers image="icon/Heltech_LoraNode151_b63b114ca3082c8d174fb4d4136e4915_13_icon.svg">
    <layer layerId="icon"/>
   </layers>
  </iconView>
  <breadboardView>
   <layers image="breadboard/Heltech_LoraNode151_b63b114ca3082c8d174fb4d4136e4915_13_breadboard.svg">
    <layer layerId="breadboard"/>
   </layers>
  </breadboardView>
  <schematicView>
   <layers image="schematic/Heltech_LoraNode151_b63b114ca3082c8d174fb4d4136e4915_13_schematic.svg">
    <layer layerId="schematic"/>
   </layers>
  </schematicView>
  <pcbView>
   <layers image="pcb/Heltech_LoraNode151_b63b114ca3082c8d174fb4d4136e4915_13_pcb.svg">
    <layer layerId="silkscreen"/>
    <layer layerId="copper0"/>
    <layer layerId="copper1"/>
   </layers>
  </pcbView>
 </views>
 <connectors>
  <connector id="connector0" type="male" name="VUSB">
   <description>VUSB 5V</description>
   <views>
    <breadboardView>
     <p terminalId="connector0terminal" svgId="connector0pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector0terminal" svgId="connector0pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector0pin" layer="copper0"/>
     <p svgId="connector0pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector1" type="male" name="GND">
   <description>GND</description>
   <views>
    <breadboardView>
     <p terminalId="connector1terminal" svgId="connector1pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector1terminal" svgId="connector1pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector1pin" layer="copper0"/>
     <p svgId="connector1pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector2" type="male" name="45">
   <description>LED SCL1 PB8 45</description>
   <views>
    <breadboardView>
     <p terminalId="connector2terminal" svgId="connector2pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector2terminal" svgId="connector2pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector2pin" layer="copper0"/>
     <p svgId="connector2pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector3" type="male" name="46">
   <description>SDA1 PB9 46</description>
   <views>
    <breadboardView>
     <p terminalId="connector3terminal" svgId="connector3pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector3terminal" svgId="connector3pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector3pin" layer="copper0"/>
     <p svgId="connector3pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector4" type="male" name="43">
   <description>RX1 SDA1 PB7 43</description>
   <views>
    <breadboardView>
     <p terminalId="connector4terminal" svgId="connector4pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector4terminal" svgId="connector4pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector4pin" layer="copper0"/>
     <p svgId="connector4pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector5" type="male" name="42">
   <description>TX1 SCL1 PB6 42</description>
   <views>
    <breadboardView>
     <p terminalId="connector5terminal" svgId="connector5pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector5terminal" svgId="connector5pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector5pin" layer="copper0"/>
     <p svgId="connector5pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector6" type="male" name="41">
   <description>MOSI3 MOSI1 PB5 41</description>
   <views>
    <breadboardView>
     <p terminalId="connector6terminal" svgId="connector6pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector6terminal" svgId="connector6pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector6pin" layer="copper0"/>
     <p svgId="connector6pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector7" type="male" name="7">
   <description>NRST 7</description>
   <views>
    <breadboardView>
     <p terminalId="connector7terminal" svgId="connector7pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector7terminal" svgId="connector7pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector7pin" layer="copper0"/>
     <p svgId="connector7pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector8" type="male" name="10">
   <description>ADC_IN0 WKUP PA0 10</description>
   <views>
    <breadboardView>
     <p terminalId="connector8terminal" svgId="connector8pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector8terminal" svgId="connector8pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector8pin" layer="copper0"/>
     <p svgId="connector8pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector9" type="male" name="11">
   <description>ADC_IN1 PA1 11</description>
   <views>
    <breadboardView>
     <p terminalId="connector9terminal" svgId="connector9pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector9terminal" svgId="connector9pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector9pin" layer="copper0"/>
     <p svgId="connector9pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector10" type="male" name="12">
   <description>Power_detection ADC_IN2 TX2 PA2 12</description>
   <views>
    <breadboardView>
     <p terminalId="connector10terminal" svgId="connector10pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector10terminal" svgId="connector10pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector10pin" layer="copper0"/>
     <p svgId="connector10pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector11" type="male" name="VEXT">
   <description>VEXT External power supply 3.3v</description>
   <views>
    <breadboardView>
     <p terminalId="connector11terminal" svgId="connector11pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector11terminal" svgId="connector11pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector11pin" layer="copper0"/>
     <p svgId="connector11pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector12" type="male" name="15">
   <description>LoRa_SCK DAC_OUT2 ADC_IN5 SCK1 PA5 15</description>
   <views>
    <breadboardView>
     <p terminalId="connector12terminal" svgId="connector12pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector12terminal" svgId="connector12pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector12pin" layer="copper0"/>
     <p svgId="connector12pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector13" type="male" name="16">
   <description>LoRa_MISO ADC_IN6 MISO1 PA6 16</description>
   <views>
    <breadboardView>
     <p terminalId="connector13terminal" svgId="connector13pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector13terminal" svgId="connector13pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector13pin" layer="copper0"/>
     <p svgId="connector13pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector14" type="male" name="17">
   <description>LoRa_MOSI ADC_IN7 MOSI1 PA7 17</description>
   <views>
    <breadboardView>
     <p terminalId="connector14terminal" svgId="connector14pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector14terminal" svgId="connector14pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector14pin" layer="copper0"/>
     <p svgId="connector14pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector15" type="male" name="14">
   <description>LoRa_NSS DAC_OUT1 ADC_IN4 NSS3 NSS1 PA4 14</description>
   <views>
    <breadboardView>
     <p terminalId="connector15terminal" svgId="connector15pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector15terminal" svgId="connector15pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector15pin" layer="copper0"/>
     <p svgId="connector15pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector16" type="male" name="18">
   <description>LoRA_DIO3 ADC_IN8 PB0 18</description>
   <views>
    <breadboardView>
     <p terminalId="connector16terminal" svgId="connector16pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector16terminal" svgId="connector16pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector16pin" layer="copper0"/>
     <p svgId="connector16pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector17" type="male" name="19">
   <description>LoRa_DIO2 ADC_IN9 PB1 19</description>
   <views>
    <breadboardView>
     <p terminalId="connector17terminal" svgId="connector17pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector17terminal" svgId="connector17pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector17pin" layer="copper0"/>
     <p svgId="connector17pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector18" type="male" name="21">
   <description>LoRa_DIO1 TX3 SCL2 PB10 21</description>
   <views>
    <breadboardView>
     <p terminalId="connector18terminal" svgId="connector18pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector18terminal" svgId="connector18pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector18pin" layer="copper0"/>
     <p svgId="connector18pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector19" type="male" name="22">
   <description>LoRa_DIO0 RX3 SDA2 PB11 22</description>
   <views>
    <breadboardView>
     <p terminalId="connector19terminal" svgId="connector19pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector19terminal" svgId="connector19pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector19pin" layer="copper0"/>
     <p svgId="connector19pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector20" type="male" name="13">
   <description>LoRa_RST ADC_IN3 RX2 PA3 13</description>
   <views>
    <breadboardView>
     <p terminalId="connector20terminal" svgId="connector20pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector20terminal" svgId="connector20pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector20pin" layer="copper0"/>
     <p svgId="connector20pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector21" type="male" name="25">
   <description>ADC_IN18 NSS2 PB12 25</description>
   <views>
    <breadboardView>
     <p terminalId="connector21terminal" svgId="connector21pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector21terminal" svgId="connector21pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector21pin" layer="copper0"/>
     <p svgId="connector21pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector22" type="male" name="40">
   <description>MISO3 MISO1 PB4 40</description>
   <views>
    <breadboardView>
     <p terminalId="connector22terminal" svgId="connector22pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector22terminal" svgId="connector22pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector22pin" layer="copper0"/>
     <p svgId="connector22pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector23" type="male" name="VEXT">
   <description>VEXT External power supply 3.3V</description>
   <views>
    <breadboardView>
     <p terminalId="connector23terminal" svgId="connector23pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector23terminal" svgId="connector23pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector23pin" layer="copper0"/>
     <p svgId="connector23pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector24" type="male" name="39">
   <description>Vext_control SCK3 SCK1 PB3 39</description>
   <views>
    <breadboardView>
     <p terminalId="connector24terminal" svgId="connector24pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector24terminal" svgId="connector24pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector24pin" layer="copper0"/>
     <p svgId="connector24pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector25" type="male" name="38">
   <description>NSS3 NSS1 PA15 38</description>
   <views>
    <breadboardView>
     <p terminalId="connector25terminal" svgId="connector25pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector25terminal" svgId="connector25pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector25pin" layer="copper0"/>
     <p svgId="connector25pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector26" type="male" name="26">
   <description>ADC_IN19 SCK2 PB13 26</description>
   <views>
    <breadboardView>
     <p terminalId="connector26terminal" svgId="connector26pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector26terminal" svgId="connector26pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector26pin" layer="copper0"/>
     <p svgId="connector26pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector27" type="male" name="27">
   <description>ADC_IN20 MISO2 PB14 27</description>
   <views>
    <breadboardView>
     <p terminalId="connector27terminal" svgId="connector27pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector27terminal" svgId="connector27pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector27pin" layer="copper0"/>
     <p svgId="connector27pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector47" type="male" name="28">
   <description>ADC_IN21 MOSI2 PB15 28</description>
   <views>
    <breadboardView>
     <p svgId="connector47pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector47pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector47pin" layer="copper0"/>
     <p svgId="connector47pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector48" type="male" name="29">
   <description>PA8 29</description>
   <views>
    <breadboardView>
     <p svgId="connector48pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector48pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector48pin" layer="copper0"/>
     <p svgId="connector48pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector49" type="male" name="30">
   <description>TX1 PA9 30</description>
   <views>
    <breadboardView>
     <p svgId="connector49pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector49pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector49pin" layer="copper0"/>
     <p svgId="connector49pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector50" type="male" name="31">
   <description>RX1 PA10 31</description>
   <views>
    <breadboardView>
     <p svgId="connector50pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector50pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector50pin" layer="copper0"/>
     <p svgId="connector50pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector51" type="male" name="34">
   <description>SWDIO PA13 34</description>
   <views>
    <breadboardView>
     <p svgId="connector51pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector51pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector51pin" layer="copper0"/>
     <p svgId="connector51pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector52" type="male" name="37">
   <description>SWCLK PA14 37</description>
   <views>
    <breadboardView>
     <p svgId="connector52pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector52pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector52pin" layer="copper0"/>
     <p svgId="connector52pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector53" type="male" name="GND">
   <description>GND</description>
   <views>
    <breadboardView>
     <p svgId="connector53pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector53pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector53pin" layer="copper0"/>
     <p svgId="connector53pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector54" type="male" name="VDD">
   <description>VDD 3.3V</description>
   <views>
    <breadboardView>
     <p svgId="connector54pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector54pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector54pin" layer="copper0"/>
     <p svgId="connector54pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
 </connectors>
 <url>https://heltec.org/project/lora-node-151/</url>
 <buses>
  <bus id="internal1">
   <nodeMember connectorId="connector53"/>
   <nodeMember connectorId="connector1"/>
  </bus>
  <bus id="internal2">
   <nodeMember connectorId="connector23"/>
   <nodeMember connectorId="connector11"/>
  </bus>
 </buses>
</module>
