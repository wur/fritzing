<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module fritzingVersion="0.9.4" moduleId="RPiHeader40p_1" referenceFile="RPiHeader40p_1.fzp">
  <version>1</version>
  <author>Jozef Sovcik (modified by vanepp Mar 2020)</author>
  <title>Raspberry Pi Header 40pin</title>
  <label>J</label>
  <date>št sep 29 2016</date>
  <url>http://www.raspberrypi.org/</url>
  <tags>
    <tag>RPi3</tag>
    <tag>RPi2B</tag>
    <tag>Raspberry</tag>
    <tag>Raspberry Pi</tag>
    <tag>RPi</tag>
  </tags>
  <properties>
    <property name="variant">variant 1</property>
    <property name="family">RPi gpio 40</property>
    <property name="Pin Spacing">0.1in (2.54mm)</property>
    <property name="Form">♂ (male)</property>
    <property name="Row">double</property>
    <property name="Pins">40</property>
    <property name="Package">THT</property>
  </properties>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;Raspberry Pi 40 pin header - for creating your own shields. (Mar 2020, bb: make breadboard friendly (.4in pin spacing) and add labels. Schematic add labels. PCB make oblong pins to allow routing traces between the pins.)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/RPiHeader40p_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/RPiHeader40p_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/RPiHeader40p_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/RPiHeader40p_1_pcb.svg">
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
        <layer layerId="silkscreen"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector type="male" id="connector0" name="3V3">
      <description>power - 3.3V</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector0pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector0pin" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector0pin"/>
          <p layer="copper1" svgId="connector0pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector1" name="5V">
      <description>power - 5V</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector1pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector1pin" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector1pin"/>
          <p layer="copper1" svgId="connector1pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector2" name="GPIO2">
      <description>SDA1 I2C</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector2pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector2pin" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector2pin"/>
          <p layer="copper1" svgId="connector2pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector3" name="5V">
      <description>power - 5V</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector3pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector3pin" terminalId="connector3terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector3pin"/>
          <p layer="copper1" svgId="connector3pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector4" name="IO3">
      <description>SCL1 I2C</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector4pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector4pin" terminalId="connector4terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector4pin"/>
          <p layer="copper1" svgId="connector4pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector5" name="GND">
      <description>Ground</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector5pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector5pin" terminalId="connector5terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector5pin"/>
          <p layer="copper1" svgId="connector5pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector6" name="IO4">
      <description>GPCLK0</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector6pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector6pin" terminalId="connector6terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector6pin"/>
          <p layer="copper1" svgId="connector6pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector7" name="IO14">
      <description>TXD</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector7pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector7pin" terminalId="connector7terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector7pin"/>
          <p layer="copper1" svgId="connector7pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector8" name="GND">
      <description>ground</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector8pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector8pin" terminalId="connector8terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector8pin"/>
          <p layer="copper1" svgId="connector8pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector9" name="IO15">
      <description>RXD</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector9pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector9pin" terminalId="connector9terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector9pin"/>
          <p layer="copper1" svgId="connector9pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector10" name="IO17">
      <description/>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector10pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector10pin" terminalId="connector10terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector10pin"/>
          <p layer="copper1" svgId="connector10pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector11" name="IO18">
      <description>PCM_CLK</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector11pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector11pin" terminalId="connector11terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector11pin"/>
          <p layer="copper1" svgId="connector11pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector12" name="IO22">
      <description/>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector12pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector12pin" terminalId="connector12terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector12pin"/>
          <p layer="copper1" svgId="connector12pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector13" name="GND">
      <description>ground</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector13pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector13pin" terminalId="connector13terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector13pin"/>
          <p layer="copper1" svgId="connector13pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector14" name="IO22">
      <description/>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector14pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector14pin" terminalId="connector14terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector14pin"/>
          <p layer="copper1" svgId="connector14pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector15" name="IO23">
      <description/>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector15pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector15pin" terminalId="connector15terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector15pin"/>
          <p layer="copper1" svgId="connector15pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector16" name="3V3">
      <description>power - 3V3</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector16pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector16pin" terminalId="connector16terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector16pin"/>
          <p layer="copper1" svgId="connector16pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector17" name="IO24">
      <description/>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector17pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector17pin" terminalId="connector17terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector17pin"/>
          <p layer="copper1" svgId="connector17pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector18" name="IO10">
      <description>MOSI</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector18pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector18pin" terminalId="connector18terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector18pin"/>
          <p layer="copper1" svgId="connector18pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector19" name="GND">
      <description>Ground</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector19pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector19pin" terminalId="connector19terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector19pin"/>
          <p layer="copper1" svgId="connector19pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector20" name="IO9">
      <description>MISO</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector20pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector20pin" terminalId="connector20terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector20pin"/>
          <p layer="copper1" svgId="connector20pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector21" name="IO25">
      <description/>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector21pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector21pin" terminalId="connector21terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector21pin"/>
          <p layer="copper1" svgId="connector21pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector22" name="IO11">
      <description>SCLK</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector22pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector22pin" terminalId="connector22terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector22pin"/>
          <p layer="copper1" svgId="connector22pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector23" name="IO8">
      <description>CE0</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector23pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector23pin" terminalId="connector23terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector23pin"/>
          <p layer="copper1" svgId="connector23pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector24" name="GND">
      <description>ground</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector24pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector24pin" terminalId="connector24terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector24pin"/>
          <p layer="copper1" svgId="connector24pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector25" name="IO7">
      <description>CE1</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector25pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector25pin" terminalId="connector25terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector25pin"/>
          <p layer="copper1" svgId="connector25pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector26" name="IO0">
      <description>ID_SD</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector26pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector26pin" terminalId="connector26terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector26pin"/>
          <p layer="copper1" svgId="connector26pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector27" name="IO1">
      <description>ID_SC</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector27pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector27pin" terminalId="connector27terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector27pin"/>
          <p layer="copper1" svgId="connector27pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector28" name="IO5">
      <description/>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector28pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector28pin" terminalId="connector28terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector28pin"/>
          <p layer="copper1" svgId="connector28pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector29" name="GND">
      <description>ground</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector29pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector29pin" terminalId="connector29terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector29pin"/>
          <p layer="copper1" svgId="connector29pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector30" name="IO6">
      <description/>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector30pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector30pin" terminalId="connector30terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector30pin"/>
          <p layer="copper1" svgId="connector30pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector31" name="IO12">
      <description>PWM0</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector31pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector31pin" terminalId="connector31terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector31pin"/>
          <p layer="copper1" svgId="connector31pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector32" name="IO13">
      <description>PWM1</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector32pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector32pin" terminalId="connector32terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector32pin"/>
          <p layer="copper1" svgId="connector32pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector33" name="GND">
      <description>Ground</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector33pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector33pin" terminalId="connector33terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector33pin"/>
          <p layer="copper1" svgId="connector33pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector34" name="IO19">
      <description>PCM_FS</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector34pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector34pin" terminalId="connector34terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector34pin"/>
          <p layer="copper1" svgId="connector34pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector35" name="IO16">
      <description/>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector35pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector35pin" terminalId="connector35terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector35pin"/>
          <p layer="copper1" svgId="connector35pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector36" name="IO26">
      <description/>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector36pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector36pin" terminalId="connector36terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector36pin"/>
          <p layer="copper1" svgId="connector36pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector37" name="IO20">
      <description>PCM_DIN</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector37pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector37pin" terminalId="connector37terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector37pin"/>
          <p layer="copper1" svgId="connector37pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector38" name="GND">
      <description>ground</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector38pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector38pin" terminalId="connector38terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector38pin"/>
          <p layer="copper1" svgId="connector38pin"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector39" name="IO21">
      <description>PCM_DOUT</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector39pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector39pin" terminalId="connector39terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector39pin"/>
          <p layer="copper1" svgId="connector39pin"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <buses>
    <bus id="5V">
      <nodeMember connectorId="connector1"/>
      <nodeMember connectorId="connector3"/>
    </bus>
    <bus id="3V3">
      <nodeMember connectorId="connector0"/>
      <nodeMember connectorId="connector16"/>
    </bus>
    <bus id="GND">
      <nodeMember connectorId="connector5"/>
      <nodeMember connectorId="connector8"/>
      <nodeMember connectorId="connector13"/>
      <nodeMember connectorId="connector19"/>
      <nodeMember connectorId="connector24"/>
      <nodeMember connectorId="connector29"/>
      <nodeMember connectorId="connector33"/>
      <nodeMember connectorId="connector38"/>
    </bus>
  </buses>
</module>
