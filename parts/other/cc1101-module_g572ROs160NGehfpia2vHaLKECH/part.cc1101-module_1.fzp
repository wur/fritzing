<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module moduleId="cc1101-module_1" referenceFile="cc1101-module_1.fzp" fritzingVersion="0.9.6">
  <version>1</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>cc1101 module</title>
  <label>M</label>
  <date>Wed Jun 9 2021</date>
  <tags>
    <tag>contrib</tag>
    <tag>cc1101 module</tag>
    <tag>433mhz</tag>
    <tag>fritzing user</tag>
    <tag>wireless</tag>
  </tags>
  <properties>
    <property name="variant">variant 1</property>
    <property name="family">cc1101 module</property>
    <property name="editable pin labels">false</property>
    <property name="part number"/>
    <property name="layer"/>
  </properties>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;CC1101 rf module.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/cc1101-module_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/cc1101-module_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/cc1101-module_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/cc1101-module_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" type="male" name="pin 1">
      <description>VDD</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" layer="schematic" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" type="male" name="pin 2">
      <description>VDD</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" layer="schematic" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" type="male" name="pin 3">
      <description>SI</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" layer="schematic" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" type="male" name="pin 4">
      <description>SCLK</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" layer="schematic" terminalId="connector3terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector4" type="male" name="pin 5">
      <description>SO</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" layer="schematic" terminalId="connector4terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector5" type="male" name="pin 6">
      <description>GDO2</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" layer="schematic" terminalId="connector5terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper0"/>
          <p svgId="connector5pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector6" type="male" name="pin 7">
      <description>CSn</description>
      <views>
        <breadboardView>
          <p svgId="connector6pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector6pin" layer="schematic" terminalId="connector6terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector6pin" layer="copper0"/>
          <p svgId="connector6pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector7" type="male" name="pin 8">
      <description>GDO0</description>
      <views>
        <breadboardView>
          <p svgId="connector7pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector7pin" layer="schematic" terminalId="connector7terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector7pin" layer="copper0"/>
          <p svgId="connector7pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector8" type="male" name="pin 9">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector8pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector8pin" layer="schematic" terminalId="connector8terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector8pin" layer="copper0"/>
          <p svgId="connector8pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector9" type="male" name="pin 10">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector9pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector9pin" layer="schematic" terminalId="connector9terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector9pin" layer="copper0"/>
          <p svgId="connector9pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <buses>
    <bus id="VDD">
      <nodeMember connectorId="connector0"/>
      <nodeMember connectorId="connector1"/>
    </bus>
    <bus id="GND">
      <nodeMember connectorId="connector8"/>
      <nodeMember connectorId="connector9"/>
    </bus>
  </buses>
</module>
