<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module moduleId="Adafruit-3104-pushbutton_1" referenceFile="Adafruit-3104-pushbutton_1.fzp" fritzingVersion="0.9.9">
  <version>1</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>Adafruit 3104 lighted pushbutton</title>
  <label>S</label>
  <date>Thu Mar 31 2022</date>
  <tags>
    <tag>contrib</tag>
    <tag>Adafruit 3104 lighted pushbutton</tag>
    <tag>switch</tag>
    <tag>fritzing user</tag>
  </tags>
  <properties>
    <property name="variant">variant 1</property>
    <property name="family">Adafruit-3104-pushbutton</property>
    <property name="editable pin labels">false</property>
    <property name="part number"/>
    <property name="layer"/>
    <property name="package">THT</property>
  </properties>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;This Mini Illuminated Momentary Power Button it an adorable breadboard-friendly tactile switch that has a built-in LED. It provides a simple way to add a tactile switch and a colorful universal power symbol to your project.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/Adafruit-3104-pushbutton_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/Adafruit-3104-pushbutton_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/Adafruit-3104-pushbutton_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/Adafruit-3104-pushbutton_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" type="male" name="pin 2">
      <description>S-1</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" layer="schematic" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" type="male" name="pin 5">
      <description>LED Cathode</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" layer="schematic" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" type="male" name="pin 1">
      <description>S-1</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" layer="schematic" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" type="male" name="pin 3">
      <description>S-2</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" layer="schematic" terminalId="connector3terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector4" type="male" name="pin 6">
      <description>LED Anode</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" layer="schematic" terminalId="connector4terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector5" type="male" name="pin 4">
      <description>S-2</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" layer="schematic" terminalId="connector5terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper0"/>
          <p svgId="connector5pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <buses>
    <bus id="S-1">
      <nodeMember connectorId="connector0"/>
      <nodeMember connectorId="connector2"/>
    </bus>
    <bus id="S-2">
      <nodeMember connectorId="connector3"/>
      <nodeMember connectorId="connector5"/>
    </bus>
  </buses>
  <url>https://www.adafruit.com/product/3104</url>
</module>
