<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module referenceFile="PCB-LQFP64_5c514768b08c4d41ba97d0a0b41f7d98_5.fzp" moduleId="PCB-LQFP64_5c514768b08c4d41ba97d0a0b41f7d98_5" fritzingVersion="0.9.4">
  <version>1</version>
  <author>HimekaAsaba (modified by vanepp May 2020)</author>
  <title>PCB-LQFP64 </title>
  <label>IC</label>
  <date>土 5 16 2020</date>
  <tags>
    <tag>DIP</tag>
  </tags>
  <properties>
    <property name="family">PCB-LQFP64</property>
    <property name="variant">variant 1</property>
    <property name="hole size">0.8mm</property>
    <property name="chip label">IC</property>
    <property name="layer"/>
    <property name="editable pin labels">false</property>
    <property name="pins">64</property>
    <property name="package">DIP (Dual Inline) [THT]</property>
    <property name="part number"/>
  </properties>
  <taxonomy>part.dip.64.pins</taxonomy>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS UI Gothic'; font-size:9pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;The PCB-LQFP64 adapter module is designed to convert integrated circuits (IC) in 64-pin QFP/LQFP/TQFP packages to a top mounted module.&lt;/p&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;This adapter was designed for engineers, students and hobbyists who are looking for a quick way of connect surface mount components to the breadboard.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/PCB-LQFP64_a752d63e28d887bbc8cd54f2caf8f11f_2_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/PCB-LQFP64_a752d63e28d887bbc8cd54f2caf8f11f_2_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/PCB-LQFP64_a752d63e28d887bbc8cd54f2caf8f11f_2_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/PCB-LQFP64_a752d63e28d887bbc8cd54f2caf8f11f_2_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" type="male" name="pin1">
      <description>pin 1</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" terminalId="connector0terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" type="male" name="pin2">
      <description>pin 2</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" terminalId="connector1terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" type="male" name="pin3">
      <description>pin 3</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" terminalId="connector2terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" type="male" name="pin4">
      <description>pin 4</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" terminalId="connector3terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector4" type="male" name="pin5">
      <description>pin 5</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" terminalId="connector4terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector5" type="male" name="pin6">
      <description>pin 6</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" terminalId="connector5terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper0"/>
          <p svgId="connector5pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector6" type="male" name="pin7">
      <description>pin 7</description>
      <views>
        <breadboardView>
          <p svgId="connector6pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector6pin" terminalId="connector6terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector6pin" layer="copper0"/>
          <p svgId="connector6pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector7" type="male" name="pin8">
      <description>pin 8</description>
      <views>
        <breadboardView>
          <p svgId="connector7pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector7pin" terminalId="connector7terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector7pin" layer="copper0"/>
          <p svgId="connector7pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector8" type="male" name="pin9">
      <description>pin 9</description>
      <views>
        <breadboardView>
          <p svgId="connector8pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector8pin" terminalId="connector8terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector8pin" layer="copper0"/>
          <p svgId="connector8pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector9" type="male" name="pin10">
      <description>pin 10</description>
      <views>
        <breadboardView>
          <p svgId="connector9pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector9pin" terminalId="connector9terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector9pin" layer="copper0"/>
          <p svgId="connector9pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector10" type="male" name="pin11">
      <description>pin 11</description>
      <views>
        <breadboardView>
          <p svgId="connector10pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector10pin" terminalId="connector10terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector10pin" layer="copper0"/>
          <p svgId="connector10pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector11" type="male" name="pin12">
      <description>pin 12</description>
      <views>
        <breadboardView>
          <p svgId="connector11pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector11pin" terminalId="connector11terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector11pin" layer="copper0"/>
          <p svgId="connector11pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector12" type="male" name="pin13">
      <description>pin 13</description>
      <views>
        <breadboardView>
          <p svgId="connector12pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector12pin" terminalId="connector12terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector12pin" layer="copper0"/>
          <p svgId="connector12pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector13" type="male" name="pin14">
      <description>pin 14</description>
      <views>
        <breadboardView>
          <p svgId="connector13pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector13pin" terminalId="connector13terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector13pin" layer="copper0"/>
          <p svgId="connector13pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector14" type="male" name="pin15">
      <description>pin 15</description>
      <views>
        <breadboardView>
          <p svgId="connector14pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector14pin" terminalId="connector14terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector14pin" layer="copper0"/>
          <p svgId="connector14pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector15" type="male" name="pin16">
      <description>pin 16</description>
      <views>
        <breadboardView>
          <p svgId="connector15pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector15pin" terminalId="connector15terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector15pin" layer="copper0"/>
          <p svgId="connector15pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector16" type="male" name="pin17">
      <description>pin 17</description>
      <views>
        <breadboardView>
          <p svgId="connector16pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector16pin" terminalId="connector16terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector16pin" layer="copper0"/>
          <p svgId="connector16pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector17" type="male" name="pin18">
      <description>pin 18</description>
      <views>
        <breadboardView>
          <p svgId="connector17pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector17pin" terminalId="connector17terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector17pin" layer="copper0"/>
          <p svgId="connector17pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector18" type="male" name="pin19">
      <description>pin 19</description>
      <views>
        <breadboardView>
          <p svgId="connector18pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector18pin" terminalId="connector18terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector18pin" layer="copper0"/>
          <p svgId="connector18pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector19" type="male" name="pin20">
      <description>pin 20</description>
      <views>
        <breadboardView>
          <p svgId="connector19pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector19pin" terminalId="connector19terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector19pin" layer="copper0"/>
          <p svgId="connector19pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector20" type="male" name="pin21">
      <description>pin 21</description>
      <views>
        <breadboardView>
          <p svgId="connector20pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector20pin" terminalId="connector20terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector20pin" layer="copper0"/>
          <p svgId="connector20pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector21" type="male" name="pin22">
      <description>pin 22</description>
      <views>
        <breadboardView>
          <p svgId="connector21pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector21pin" terminalId="connector21terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector21pin" layer="copper0"/>
          <p svgId="connector21pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector22" type="male" name="pin23">
      <description>pin 23</description>
      <views>
        <breadboardView>
          <p svgId="connector22pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector22pin" terminalId="connector22terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector22pin" layer="copper0"/>
          <p svgId="connector22pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector23" type="male" name="pin24">
      <description>pin 24</description>
      <views>
        <breadboardView>
          <p svgId="connector23pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector23pin" terminalId="connector23terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector23pin" layer="copper0"/>
          <p svgId="connector23pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector24" type="male" name="pin25">
      <description>pin 25</description>
      <views>
        <breadboardView>
          <p svgId="connector24pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector24pin" terminalId="connector24terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector24pin" layer="copper0"/>
          <p svgId="connector24pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector25" type="male" name="pin26">
      <description>pin 26</description>
      <views>
        <breadboardView>
          <p svgId="connector25pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector25pin" terminalId="connector25terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector25pin" layer="copper0"/>
          <p svgId="connector25pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector26" type="male" name="pin27">
      <description>pin 27</description>
      <views>
        <breadboardView>
          <p svgId="connector26pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector26pin" terminalId="connector26terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector26pin" layer="copper0"/>
          <p svgId="connector26pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector27" type="male" name="pin28">
      <description>pin 28</description>
      <views>
        <breadboardView>
          <p svgId="connector27pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector27pin" terminalId="connector27terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector27pin" layer="copper0"/>
          <p svgId="connector27pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector28" type="male" name="pin29">
      <description>pin 29</description>
      <views>
        <breadboardView>
          <p svgId="connector28pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector28pin" terminalId="connector28terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector28pin" layer="copper0"/>
          <p svgId="connector28pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector29" type="male" name="pin30">
      <description>pin 30</description>
      <views>
        <breadboardView>
          <p svgId="connector29pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector29pin" terminalId="connector29terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector29pin" layer="copper0"/>
          <p svgId="connector29pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector30" type="male" name="pin31">
      <description>pin 31</description>
      <views>
        <breadboardView>
          <p svgId="connector30pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector30pin" terminalId="connector30terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector30pin" layer="copper0"/>
          <p svgId="connector30pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector31" type="male" name="pin32">
      <description>pin 32</description>
      <views>
        <breadboardView>
          <p svgId="connector31pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector31pin" terminalId="connector31terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector31pin" layer="copper0"/>
          <p svgId="connector31pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector32" type="male" name="pin33">
      <description>pin 33</description>
      <views>
        <breadboardView>
          <p svgId="connector32pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector32pin" terminalId="connector32terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector32pin" layer="copper0"/>
          <p svgId="connector32pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector33" type="male" name="pin34">
      <description>pin 34</description>
      <views>
        <breadboardView>
          <p svgId="connector33pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector33pin" terminalId="connector33terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector33pin" layer="copper0"/>
          <p svgId="connector33pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector34" type="male" name="pin35">
      <description>pin 35</description>
      <views>
        <breadboardView>
          <p svgId="connector34pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector34pin" terminalId="connector34terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector34pin" layer="copper0"/>
          <p svgId="connector34pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector35" type="male" name="pin36">
      <description>pin 36</description>
      <views>
        <breadboardView>
          <p svgId="connector35pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector35pin" terminalId="connector35terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector35pin" layer="copper0"/>
          <p svgId="connector35pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector36" type="male" name="pin37">
      <description>pin 37</description>
      <views>
        <breadboardView>
          <p svgId="connector36pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector36pin" terminalId="connector36terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector36pin" layer="copper0"/>
          <p svgId="connector36pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector37" type="male" name="pin38">
      <description>pin 38</description>
      <views>
        <breadboardView>
          <p svgId="connector37pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector37pin" terminalId="connector37terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector37pin" layer="copper0"/>
          <p svgId="connector37pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector38" type="male" name="pin39">
      <description>pin 39</description>
      <views>
        <breadboardView>
          <p svgId="connector38pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector38pin" terminalId="connector38terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector38pin" layer="copper0"/>
          <p svgId="connector38pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector39" type="male" name="pin40">
      <description>pin 40</description>
      <views>
        <breadboardView>
          <p svgId="connector39pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector39pin" terminalId="connector39terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector39pin" layer="copper0"/>
          <p svgId="connector39pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector40" type="male" name="pin41">
      <description>pin 41</description>
      <views>
        <breadboardView>
          <p svgId="connector40pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector40pin" terminalId="connector40terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector40pin" layer="copper0"/>
          <p svgId="connector40pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector41" type="male" name="pin42">
      <description>pin 42</description>
      <views>
        <breadboardView>
          <p svgId="connector41pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector41pin" terminalId="connector41terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector41pin" layer="copper0"/>
          <p svgId="connector41pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector42" type="male" name="pin43">
      <description>pin 43</description>
      <views>
        <breadboardView>
          <p svgId="connector42pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector42pin" terminalId="connector42terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector42pin" layer="copper0"/>
          <p svgId="connector42pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector43" type="male" name="pin44">
      <description>pin 44</description>
      <views>
        <breadboardView>
          <p svgId="connector43pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector43pin" terminalId="connector43terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector43pin" layer="copper0"/>
          <p svgId="connector43pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector44" type="male" name="pin45">
      <description>pin 45</description>
      <views>
        <breadboardView>
          <p svgId="connector44pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector44pin" terminalId="connector44terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector44pin" layer="copper0"/>
          <p svgId="connector44pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector45" type="male" name="pin46">
      <description>pin 46</description>
      <views>
        <breadboardView>
          <p svgId="connector45pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector45pin" terminalId="connector45terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector45pin" layer="copper0"/>
          <p svgId="connector45pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector46" type="male" name="pin47">
      <description>pin 47</description>
      <views>
        <breadboardView>
          <p svgId="connector46pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector46pin" terminalId="connector46terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector46pin" layer="copper0"/>
          <p svgId="connector46pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector47" type="male" name="pin48">
      <description>pin 48</description>
      <views>
        <breadboardView>
          <p svgId="connector47pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector47pin" terminalId="connector47terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector47pin" layer="copper0"/>
          <p svgId="connector47pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector48" type="male" name="pin49">
      <description>pin 49</description>
      <views>
        <breadboardView>
          <p svgId="connector48pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector48pin" terminalId="connector48terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector48pin" layer="copper0"/>
          <p svgId="connector48pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector49" type="male" name="pin50">
      <description>pin 50</description>
      <views>
        <breadboardView>
          <p svgId="connector49pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector49pin" terminalId="connector49terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector49pin" layer="copper0"/>
          <p svgId="connector49pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector50" type="male" name="pin51">
      <description>pin 51</description>
      <views>
        <breadboardView>
          <p svgId="connector50pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector50pin" terminalId="connector50terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector50pin" layer="copper0"/>
          <p svgId="connector50pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector51" type="male" name="pin52">
      <description>pin 52</description>
      <views>
        <breadboardView>
          <p svgId="connector51pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector51pin" terminalId="connector51terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector51pin" layer="copper0"/>
          <p svgId="connector51pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector52" type="male" name="pin53">
      <description>pin 53</description>
      <views>
        <breadboardView>
          <p svgId="connector52pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector52pin" terminalId="connector52terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector52pin" layer="copper0"/>
          <p svgId="connector52pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector53" type="male" name="pin54">
      <description>pin 54</description>
      <views>
        <breadboardView>
          <p svgId="connector53pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector53pin" terminalId="connector53terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector53pin" layer="copper0"/>
          <p svgId="connector53pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector54" type="male" name="pin55">
      <description>pin 55</description>
      <views>
        <breadboardView>
          <p svgId="connector54pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector54pin" terminalId="connector54terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector54pin" layer="copper0"/>
          <p svgId="connector54pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector55" type="male" name="pin56">
      <description>pin 56</description>
      <views>
        <breadboardView>
          <p svgId="connector55pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector55pin" terminalId="connector55terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector55pin" layer="copper0"/>
          <p svgId="connector55pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector56" type="male" name="pin57">
      <description>pin 57</description>
      <views>
        <breadboardView>
          <p svgId="connector56pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector56pin" terminalId="connector56terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector56pin" layer="copper0"/>
          <p svgId="connector56pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector57" type="male" name="pin58">
      <description>pin 58</description>
      <views>
        <breadboardView>
          <p svgId="connector57pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector57pin" terminalId="connector57terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector57pin" layer="copper0"/>
          <p svgId="connector57pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector58" type="male" name="pin59">
      <description>pin 59</description>
      <views>
        <breadboardView>
          <p svgId="connector58pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector58pin" terminalId="connector58terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector58pin" layer="copper0"/>
          <p svgId="connector58pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector59" type="male" name="pin60">
      <description>pin 60</description>
      <views>
        <breadboardView>
          <p svgId="connector59pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector59pin" terminalId="connector59terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector59pin" layer="copper0"/>
          <p svgId="connector59pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector60" type="male" name="pin61">
      <description>pin 61</description>
      <views>
        <breadboardView>
          <p svgId="connector60pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector60pin" terminalId="connector60terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector60pin" layer="copper0"/>
          <p svgId="connector60pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector61" type="male" name="pin62">
      <description>pin 62</description>
      <views>
        <breadboardView>
          <p svgId="connector61pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector61pin" terminalId="connector61terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector61pin" layer="copper0"/>
          <p svgId="connector61pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector62" type="male" name="pin63">
      <description>pin 63</description>
      <views>
        <breadboardView>
          <p svgId="connector62pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector62pin" terminalId="connector62terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector62pin" layer="copper0"/>
          <p svgId="connector62pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector63" type="male" name="pin64">
      <description>pin 64</description>
      <views>
        <breadboardView>
          <p svgId="connector63pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector63pin" terminalId="connector63terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector63pin" layer="copper0"/>
          <p svgId="connector63pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
</module>
