<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module fritzingVersion="0.9.7" moduleId="gy-max30102-module_1" referenceFile="gy-max30102-module_1.fzp">
  <version>1</version>
  <date>Tue Aug 3 2021</date>
  <author>Peter Van Epp (vanepp in forums)</author>
  <description>Oximeter module using the MAX30102 chip. You may wish to read (and check your module) this post on power problems with these modules: https://reedpaper.wordpress.com/2018/08/22/pulse-oximeter-max30100-max30102-how-to-fix-wrong-board/ (may not apply as this is a different module!). As usual, if you want the mounting holes in pcb, you need to drag a hole over the pad in silkscreen to actually drill the hole (by default they are not drilled)</description>
  <title>GY-MAX30102</title>
  <tags>
    <tag>Oximeter</tag>
    <tag>contrib</tag>
    <tag>MAX30102</tag>
    <tag>Heart Rate Sensor</tag>
  </tags>
  <properties>
    <property name="family">GY-Oximeter</property>
    <property name="variant">variant 1</property>
    <property name="layer"/>
  </properties>
  <views>
    <breadboardView>
      <layers image="breadboard/gy-max30102-module_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/gy-max30102-module_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/gy-max30102-module_1_pcb.svg">
        <layer layerId="copper1"/>
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
      </layers>
    </pcbView>
    <iconView>
      <layers image="breadboard/gy-max30102-module_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
  </views>
  <connectors>
    <connector id="connector0" name="VIN" type="male">
      <description>VIN</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" terminalId="connector0terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" name="GND" type="male">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" terminalId="connector1terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" name="SCL" type="male">
      <description>SCL</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" terminalId="connector2terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" name="SDA" type="male">
      <description>SDA</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" terminalId="connector3terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector4" name="INT" type="male">
      <description>INT</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" terminalId="connector4terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <buses/>
  <url/>
  <label>M</label>
</module>
