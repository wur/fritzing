<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module fritzingVersion="0.9.9" moduleId="makerfabs-esp32-uwb_1">
  <version>1</version>
  <date>Sat Mar 12 2022</date>
  <author>Peter Van Epp (vanepp in forums)</author>
  <description>Makerfabs ESP32 UWB module is based on DW1000, it acts like a continuously scanning radar, that precisely locks onto another device (called Anchor) and communicates with it, thus calculating its own location.</description>
  <title>ESP32 UWB v1.0</title>
  <label>A</label>
  <tags>
    <tag>contrib</tag>
    <tag>fritzing user</tag>
    <tag>Makerfabs ESP32 UWB module</tag>
    <tag>ESP32 UWB v1.0</tag>
  </tags>
  <properties>
    <property name="variant">variant 1</property>
    <property name="family">Makerfabs ESP32 UWB module</property>
    <property name="editable pin labels">false</property>
    <property name="part number"/>
    <property name="layer"/>
  </properties>
  <views>
    <breadboardView>
      <layers image="breadboard/makerfabs-esp32-uwb_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/makerfabs-esp32-uwb_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/makerfabs-esp32-uwb_1_pcb.svg">
        <layer layerId="copper1"/>
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
      </layers>
    </pcbView>
    <iconView>
      <layers image="breadboard/makerfabs-esp32-uwb_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
  </views>
  <connectors>
    <connector id="connector0" type="male" name="+3V3">
      <description>+3V3</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" layer="schematic" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" type="male" name="GND">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" layer="schematic" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" type="male" name="RESET">
      <description>RESET</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" layer="schematic" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" type="male" name="GND">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" layer="schematic" terminalId="connector3terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector4" type="male" name="IO2">
      <description>IO2</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" layer="schematic" terminalId="connector4terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector5" type="male" name="IO12/MISO">
      <description>IO12/MISO</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" layer="schematic" terminalId="connector5terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper0"/>
          <p svgId="connector5pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector6" type="male" name="IO13/MOSI">
      <description>IO13/MOSI</description>
      <views>
        <breadboardView>
          <p svgId="connector6pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector6pin" layer="schematic" terminalId="connector6terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector6pin" layer="copper0"/>
          <p svgId="connector6pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector7" type="male" name="IO14/SCLK">
      <description>IO14/SCLK</description>
      <views>
        <breadboardView>
          <p svgId="connector7pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector7pin" layer="schematic" terminalId="connector7terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector7pin" layer="copper0"/>
          <p svgId="connector7pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector8" type="male" name="IO15/NSS">
      <description>IO15/NSS</description>
      <views>
        <breadboardView>
          <p svgId="connector8pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector8pin" layer="schematic" terminalId="connector8terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector8pin" layer="copper0"/>
          <p svgId="connector8pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector9" type="male" name="IO18">
      <description>IO18</description>
      <views>
        <breadboardView>
          <p svgId="connector9pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector9pin" layer="schematic" terminalId="connector9terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector9pin" layer="copper0"/>
          <p svgId="connector9pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector10" type="male" name="IO19">
      <description>IO19</description>
      <views>
        <breadboardView>
          <p svgId="connector10pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector10pin" layer="schematic" terminalId="connector10terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector10pin" layer="copper0"/>
          <p svgId="connector10pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector11" type="male" name="IO25">
      <description>IO25</description>
      <views>
        <breadboardView>
          <p svgId="connector11pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector11pin" layer="schematic" terminalId="connector11terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector11pin" layer="copper0"/>
          <p svgId="connector11pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector12" type="male" name="IO26">
      <description>IO26</description>
      <views>
        <breadboardView>
          <p svgId="connector12pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector12pin" layer="schematic" terminalId="connector12terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector12pin" layer="copper0"/>
          <p svgId="connector12pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector13" type="male" name="IO27">
      <description>IO27</description>
      <views>
        <breadboardView>
          <p svgId="connector13pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector13pin" layer="schematic" terminalId="connector13terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector13pin" layer="copper0"/>
          <p svgId="connector13pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector14" type="male" name="IO32">
      <description>IO32</description>
      <views>
        <breadboardView>
          <p svgId="connector14pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector14pin" layer="schematic" terminalId="connector14terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector14pin" layer="copper0"/>
          <p svgId="connector14pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector15" type="male" name="IO33">
      <description>IO33</description>
      <views>
        <breadboardView>
          <p svgId="connector15pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector15pin" layer="schematic" terminalId="connector15terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector15pin" layer="copper0"/>
          <p svgId="connector15pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector16" type="male" name="IO34">
      <description>IO34</description>
      <views>
        <breadboardView>
          <p svgId="connector16pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector16pin" layer="schematic" terminalId="connector16terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector16pin" layer="copper0"/>
          <p svgId="connector16pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector17" type="male" name="IO35">
      <description>IO35</description>
      <views>
        <breadboardView>
          <p svgId="connector17pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector17pin" layer="schematic" terminalId="connector17terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector17pin" layer="copper0"/>
          <p svgId="connector17pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector18" type="male" name="IO36/SVP">
      <description>IO36/SVP</description>
      <views>
        <breadboardView>
          <p svgId="connector18pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector18pin" layer="schematic" terminalId="connector18terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector18pin" layer="copper0"/>
          <p svgId="connector18pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector19" type="male" name="IO39/SVN">
      <description>IO39/SVN</description>
      <views>
        <breadboardView>
          <p svgId="connector19pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector19pin" layer="schematic" terminalId="connector19terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector19pin" layer="copper0"/>
          <p svgId="connector19pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector20" type="male" name="IO23">
      <description>IO23</description>
      <views>
        <breadboardView>
          <p svgId="connector20pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector20pin" layer="schematic" terminalId="connector20terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector20pin" layer="copper0"/>
          <p svgId="connector20pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector21" type="male" name="IO22/DC">
      <description>IO22/DC</description>
      <views>
        <breadboardView>
          <p svgId="connector21pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector21pin" layer="schematic" terminalId="connector21terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector21pin" layer="copper0"/>
          <p svgId="connector21pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector22" type="male" name="IO21/RES">
      <description>IO21/RES</description>
      <views>
        <breadboardView>
          <p svgId="connector22pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector22pin" layer="schematic" terminalId="connector22terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector22pin" layer="copper0"/>
          <p svgId="connector22pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector23" type="male" name="IO17">
      <description>IO17</description>
      <views>
        <breadboardView>
          <p svgId="connector23pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector23pin" layer="schematic" terminalId="connector23terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector23pin" layer="copper0"/>
          <p svgId="connector23pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector24" type="male" name="IO16">
      <description>IO16</description>
      <views>
        <breadboardView>
          <p svgId="connector24pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector24pin" layer="schematic" terminalId="connector24terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector24pin" layer="copper0"/>
          <p svgId="connector24pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector25" type="male" name="IO5">
      <description>IO5</description>
      <views>
        <breadboardView>
          <p svgId="connector25pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector25pin" layer="schematic" terminalId="connector25terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector25pin" layer="copper0"/>
          <p svgId="connector25pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector26" type="male" name="IO4">
      <description>IO4</description>
      <views>
        <breadboardView>
          <p svgId="connector26pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector26pin" layer="schematic" terminalId="connector26terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector26pin" layer="copper0"/>
          <p svgId="connector26pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector27" type="male" name="IO0">
      <description>IO0</description>
      <views>
        <breadboardView>
          <p svgId="connector27pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector27pin" layer="schematic" terminalId="connector27terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector27pin" layer="copper0"/>
          <p svgId="connector27pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector28" type="male" name="TXD">
      <description>TXD</description>
      <views>
        <breadboardView>
          <p svgId="connector28pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector28pin" layer="schematic" terminalId="connector28terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector28pin" layer="copper0"/>
          <p svgId="connector28pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector29" type="male" name="RXD">
      <description>RXD</description>
      <views>
        <breadboardView>
          <p svgId="connector29pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector29pin" layer="schematic" terminalId="connector29terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector29pin" layer="copper0"/>
          <p svgId="connector29pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector30" type="male" name="GND">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector30pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector30pin" layer="schematic" terminalId="connector30terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector30pin" layer="copper0"/>
          <p svgId="connector30pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector31" type="male" name="VDD5V">
      <description>VDD5V</description>
      <views>
        <breadboardView>
          <p svgId="connector31pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector31pin" layer="schematic" terminalId="connector31terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector31pin" layer="copper0"/>
          <p svgId="connector31pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <buses>
    <bus id="GND">
      <nodeMember connectorId="connector1"/>
      <nodeMember connectorId="connector3"/>
      <nodeMember connectorId="connector30"/>
    </bus>
  </buses>
  <url>https://www.makerfabs.com/esp32-uwb-ultra-wideband.html</url>
</module>
