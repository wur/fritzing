<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module moduleId="prefix0000_5ecb895cf67812191e7f03d069e1d6d6_2" referenceFile="voltammeter_1.fzp" fritzingVersion="0.9.6">
 <version>1</version>
 <author>Peter Van Epp (vanepp in forums)</author>
 <title>volt-ammeter</title>
 <label>M</label>
 <date>Sat May 1 2021</date>
 <tags>
  <tag>contrib</tag>
  <tag>voltmeter</tag>
  <tag>ammeter</tag>
  <tag>fritzing user</tag>
 </tags>
 <properties>
  <property name="variant">variant 2</property>
  <property name="family">volt-ammeter</property>
  <property name="editable pin labels">false</property>
  <property name="part number"></property>
  <property name="layer"></property>
  <property name="package">THT</property>
 </properties>
 <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
&lt;html>&lt;head>&lt;meta name="qrichtext" content="1" />&lt;style type="text/css">
p, li { white-space: pre-wrap; }
&lt;/style>&lt;/head>&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;">
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">Volt-ammeter 0-100V 0-10A panel mount.&lt;/p>&lt;/body>&lt;/html></description>
 <views>
  <iconView>
   <layers image="icon/prefix0000_5ecb895cf67812191e7f03d069e1d6d6_2_icon.svg">
    <layer layerId="icon"/>
   </layers>
  </iconView>
  <breadboardView>
   <layers image="breadboard/prefix0000_5ecb895cf67812191e7f03d069e1d6d6_2_breadboard.svg">
    <layer layerId="breadboard"/>
   </layers>
  </breadboardView>
  <schematicView>
   <layers image="schematic/prefix0000_5ecb895cf67812191e7f03d069e1d6d6_2_schematic.svg">
    <layer layerId="schematic"/>
   </layers>
  </schematicView>
  <pcbView>
   <layers image="pcb/prefix0000_5ecb895cf67812191e7f03d069e1d6d6_2_pcb.svg">
    <layer layerId="silkscreen"/>
    <layer layerId="copper0"/>
    <layer layerId="copper1"/>
   </layers>
  </pcbView>
 </views>
 <connectors>
  <connector id="connector0" type="male" name="-PWR">
   <description>Input power ground (one end of the current shunt) </description>
   <views>
    <breadboardView>
     <p svgId="connector0pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector0pin" terminalId="connector0terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector0pin" layer="copper0"/>
     <p svgId="connector0pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector1" type="male" name="-LOAD">
   <description>Load end of the current shunt to the load ground</description>
   <views>
    <breadboardView>
     <p svgId="connector1pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector1pin" terminalId="connector1terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector1pin" layer="copper0"/>
     <p svgId="connector1pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector2" type="male" name="LOAD V+">
   <description>Load voltage sense to the + terminal of the load</description>
   <views>
    <breadboardView>
     <p svgId="connector2pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector2pin" terminalId="connector2terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector2pin" layer="copper0"/>
     <p svgId="connector2pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector3" type="male" name="Meter +">
   <description>VCC for the meter</description>
   <views>
    <breadboardView>
     <p svgId="connector3pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector3pin" terminalId="connector3terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector3pin" layer="copper0"/>
     <p svgId="connector3pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector4" type="male" name="Meter -">
   <description>ground for the meter (connected to load ground)</description>
   <views>
    <breadboardView>
     <p svgId="connector4pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector4pin" terminalId="connector4terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector4pin" layer="copper0"/>
     <p svgId="connector4pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
 </connectors>
 <buses>
  <bus id="GND">
   <nodeMember connectorId="connector0"/>
   <nodeMember connectorId="connector4"/>
  </bus>
 </buses>
</module>
