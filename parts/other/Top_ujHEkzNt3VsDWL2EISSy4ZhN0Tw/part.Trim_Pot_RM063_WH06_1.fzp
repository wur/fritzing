<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module moduleId="Trim_Pot_RM063_WH06_1" referenceFile="Trimmer-Potentiometer-middle-pin-bottom_1.fzp" fritzingVersion="0.9.9">
  <version>4</version>
  <author>Peter Van Epp (vanepp in forums), arnab.blue</author>
  <title>Trimmer Potentiometer middle pin bottom</title>
  <label>R</label>
  <date>Sat Oct 16 2021</date>
  <tags>
    <tag>potentiometer</tag>
    <tag>RM063</tag>
    <tag>adjustable</tag>
    <tag>resistor</tag>
    <tag>fritzing user</tag>
  </tags>
  <properties>
    <property name="family">potentiometer</property>
    <property name="type">Trimmer Potentiometer middle pin bottom</property>
    <property showInLabel="yes" name="Maximum Resistance">10kΩ</property>
    <property name="Track">Linear</property>
    <property showInLabel="yes" name="power"/>
    <property name="Size">Trimmer - 6mm</property>
    <property name="package">THT</property>
    <property name="layer"/>
    <property name="part number"/>
    <property name="variant">variant 1</property>
  </properties>
  <taxonomy>discreteParts.resistors.adjustable.potentiometer</taxonomy>
  <description>A standard 6mm trimmer potentiometer with the middle pin on the bottom rather than the top.</description>
  <views>
    <iconView>
      <layers image="breadboard/Trim_Pot_RM063_WH06_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/Trim_Pot_RM063_WH06_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView flipvertical="true" fliphorizontal="true">
      <layers image="schematic/Trim_Pot_RM063_WH06_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/Trim_Pot_RM063_WH06_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector type="male" id="connector0" name="leg1">
      <description>leg1</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector0pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" terminalId="connector0terminal" svgId="connector0pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector0pad"/>
          <p layer="copper1" svgId="connector0pad"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector1" name="wiper">
      <description>wiper</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector1pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" terminalId="connector1terminal" svgId="connector1pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector1pad"/>
          <p layer="copper1" svgId="connector1pad"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector2" name="leg2">
      <description>leg2</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector2pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" terminalId="connector2terminal" svgId="connector2pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector2pad"/>
          <p layer="copper1" svgId="connector2pad"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
</module>
