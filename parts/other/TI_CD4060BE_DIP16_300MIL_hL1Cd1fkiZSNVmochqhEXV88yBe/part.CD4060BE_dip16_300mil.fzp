<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module fritzingVersion="0.9.4" moduleId="CD4060BE_dip16_300mil" referenceFile="CD4060BE_dip16_300mil.fzp">
  <version>4</version>
  <author>TEXAS INSTRUMENTS</author>
  <title>14 STAGE BINARY COUNTER</title>
  <label>CD4060BE-</label>
  <date>Mon Feb 10 2020</date>
  <tags>
    <tag>DIP</tag>
    <tag>Fritzing user</tag>
    <tag>CD4060</tag>
    <tag>Binary Counter</tag>
    <tag>Texas Instruments</tag>
    <tag>Through Hole</tag>
  </tags>
  <properties>
    <property name="chip label">CD4060BE</property>
    <property name="hole size">0.030in</property>
    <property name="editable pin labels">true</property>
    <property name="family">BINARY COUNTER</property>
    <property name="pins">16</property>
    <property name="part number">CD4060BE</property>
    <property name="layer"/>
    <property name="pin spacing">300mil</property>
    <property name="variant">variant 1</property>
    <property name="package">DIP (Dual Inline) [THT]</property>
  </properties>
  <description>TI CD4060BE 14 Stage Binary Counter with 16 pins on a 300mil dual inline package (DIP) footprint.</description>
  <views>
    <iconView>
      <layers image="icon/CD4060BE_dip16_300mil_icon.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/CD4060BE_dip16_300mil_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <pcbView>
      <layers image="pcb/CD4060BE_dip16_300mil_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
    <schematicView>
      <layers image="schematic/CD4060BE_dip16_300mil_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
  </views>
  <connectors>
    <connector id="connector0" name="Q12" type="male">
      <description>pin 1</description>
      <views>
        <breadboardView>
          <p terminalId="connector0terminal" layer="breadboard" svgId="connector0pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector0terminal" layer="schematic" svgId="connector0pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector0pin"/>
          <p layer="copper1" svgId="connector0pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" name="Q13" type="male">
      <description>pin 2</description>
      <views>
        <breadboardView>
          <p terminalId="connector1terminal" layer="breadboard" svgId="connector1pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector1terminal" layer="schematic" svgId="connector1pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector1pin"/>
          <p layer="copper1" svgId="connector1pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" name="Q14" type="male">
      <description>pin 3</description>
      <views>
        <breadboardView>
          <p terminalId="connector2terminal" layer="breadboard" svgId="connector2pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector2terminal" layer="schematic" svgId="connector2pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector2pin"/>
          <p layer="copper1" svgId="connector2pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" name="Q6" type="male">
      <description>pin 4</description>
      <views>
        <breadboardView>
          <p terminalId="connector3terminal" layer="breadboard" svgId="connector3pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector3terminal" layer="schematic" svgId="connector3pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector3pin"/>
          <p layer="copper1" svgId="connector3pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector4" name="Q5" type="male">
      <description>pin 5</description>
      <views>
        <breadboardView>
          <p terminalId="connector4terminal" layer="breadboard" svgId="connector4pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector4terminal" layer="schematic" svgId="connector4pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector4pin"/>
          <p layer="copper1" svgId="connector4pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector5" name="Q7" type="male">
      <description>pin 6</description>
      <views>
        <breadboardView>
          <p terminalId="connector5terminal" layer="breadboard" svgId="connector5pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector5terminal" layer="schematic" svgId="connector5pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector5pin"/>
          <p layer="copper1" svgId="connector5pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector6" name="Q4" type="male">
      <description>pin 7</description>
      <views>
        <breadboardView>
          <p terminalId="connector6terminal" layer="breadboard" svgId="connector6pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector6terminal" layer="schematic" svgId="connector6pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector6pin"/>
          <p layer="copper1" svgId="connector6pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector7" name="GND" type="male">
      <description>pin 8</description>
      <views>
        <breadboardView>
          <p terminalId="connector7terminal" layer="breadboard" svgId="connector7pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector7terminal" layer="schematic" svgId="connector7pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector7pin"/>
          <p layer="copper1" svgId="connector7pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector8" name="CLK_OUT" type="male">
      <description>pin 9</description>
      <views>
        <breadboardView>
          <p terminalId="connector8terminal" layer="breadboard" svgId="connector8pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector8terminal" layer="schematic" svgId="connector8pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector8pin"/>
          <p layer="copper1" svgId="connector8pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector9" name="CLK_OUT '" type="male">
      <description>pin 10</description>
      <views>
        <breadboardView>
          <p terminalId="connector9terminal" layer="breadboard" svgId="connector9pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector9terminal" layer="schematic" svgId="connector9pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector9pin"/>
          <p layer="copper1" svgId="connector9pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector10" name="CLK_IN" type="male">
      <description>pin 11</description>
      <views>
        <breadboardView>
          <p terminalId="connector10terminal" layer="breadboard" svgId="connector10pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector10terminal" layer="schematic" svgId="connector10pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector10pin"/>
          <p layer="copper1" svgId="connector10pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector11" name="RST" type="male">
      <description>pin 12</description>
      <views>
        <breadboardView>
          <p terminalId="connector11terminal" layer="breadboard" svgId="connector11pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector11terminal" layer="schematic" svgId="connector11pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector11pin"/>
          <p layer="copper1" svgId="connector11pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector12" name="Q9" type="male">
      <description>pin 13</description>
      <views>
        <breadboardView>
          <p terminalId="connector12terminal" layer="breadboard" svgId="connector12pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector12terminal" layer="schematic" svgId="connector12pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector12pin"/>
          <p layer="copper1" svgId="connector12pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector13" name="Q8" type="male">
      <description>pin 14</description>
      <views>
        <breadboardView>
          <p terminalId="connector13terminal" layer="breadboard" svgId="connector13pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector13terminal" layer="schematic" svgId="connector13pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector13pin"/>
          <p layer="copper1" svgId="connector13pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector14" name="Q10" type="male">
      <description>pin 15</description>
      <views>
        <breadboardView>
          <p terminalId="connector14terminal" layer="breadboard" svgId="connector14pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector14terminal" layer="schematic" svgId="connector14pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector14pin"/>
          <p layer="copper1" svgId="connector14pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector15" name="+Vcc" type="male">
      <description>pin 16</description>
      <views>
        <breadboardView>
          <p terminalId="connector15terminal" layer="breadboard" svgId="connector15pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector15terminal" layer="schematic" svgId="connector15pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector15pin"/>
          <p layer="copper1" svgId="connector15pin"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <url>https://www.ti.com/lit/ds/symlink/cd4060b.pdf</url>
</module>
