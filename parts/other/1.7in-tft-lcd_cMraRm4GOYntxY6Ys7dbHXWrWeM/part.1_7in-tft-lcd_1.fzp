<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module referenceFile="1_7in-tft-lcd_1.fzp" moduleId="1_7in-tft-lcd_1" fritzingVersion="0.9.6">
  <version>1</version>
  <date>Fri May 14 2021</date>
  <author>Peter Van Epp (vanepp in forums)</author>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;&lt;span style=" font-family:'Helvetica'; font-size:14pt;"&gt;This is the Zoll 1.77in spi lcd display. &lt;/span&gt;&lt;/p&gt;
&lt;p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Helvetica'; font-size:14pt;"&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;&lt;span style=" font-family:'Helvetica'; font-size:14pt;"&gt;The 1.77&amp;quot; display has 128x160 color pixels.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <title>1_7in tft lcd</title>
  <tags>
    <tag>fritzing user</tag>
    <tag>contrib</tag>
    <tag>160x128</tag>
    <tag>tft rgb display</tag>
    <tag>1.77 inch</tag>
  </tags>
  <properties>
    <property name="family">1_7in-tft-lcd</property>
    <property name="variant">160x128</property>
    <property name="layer"/>
  </properties>
  <views>
    <breadboardView>
      <layers image="breadboard/1_7in-tft-lcd_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/1_7in-tft-lcd_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/1_7in-tft-lcd_1_pcb.svg">
        <layer layerId="copper1"/>
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
      </layers>
    </pcbView>
    <iconView>
      <layers image="breadboard/1_7in-tft-lcd_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
  </views>
  <connectors>
    <connector name="pin 1" type="male" id="connector0">
      <description>GND</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector0pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector0terminal" layer="schematic" svgId="connector0pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector0pin"/>
          <p layer="copper1" svgId="connector0pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin2" type="male" id="connector1">
      <description>VCC</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector1pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector1terminal" layer="schematic" svgId="connector1pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector1pin"/>
          <p layer="copper1" svgId="connector1pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin3" type="male" id="connector2">
      <description>SCL</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector2pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector2terminal" layer="schematic" svgId="connector2pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector2pin"/>
          <p layer="copper1" svgId="connector2pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin4" type="male" id="connector3">
      <description>SDA</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector3pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector3terminal" layer="schematic" svgId="connector3pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector3pin"/>
          <p layer="copper1" svgId="connector3pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin 5" type="male" id="connector4">
      <description>RES</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector4pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector4terminal" layer="schematic" svgId="connector4pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector4pin"/>
          <p layer="copper1" svgId="connector4pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin 6" type="male" id="connector5">
      <description>RS</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector5pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector5terminal" layer="schematic" svgId="connector5pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector5pin"/>
          <p layer="copper1" svgId="connector5pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin 7" type="male" id="connector6">
      <description>CS</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector6pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector6terminal" layer="schematic" svgId="connector6pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector6pin"/>
          <p layer="copper1" svgId="connector6pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin 8" type="male" id="connector7">
      <description>LED A</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector7pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector7terminal" layer="schematic" svgId="connector7pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector7pin"/>
          <p layer="copper1" svgId="connector7pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin 9" type="male" id="connector8">
      <description>LED A</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector8pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector8terminal" layer="schematic" svgId="connector8pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector8pin"/>
          <p layer="copper1" svgId="connector8pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin 10" type="male" id="connector9">
      <description>GT_CS</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector9pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector9terminal" layer="schematic" svgId="connector9pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector9pin"/>
          <p layer="copper1" svgId="connector9pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin 11" type="male" id="connector10">
      <description>GT_SI</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector10pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector10terminal" layer="schematic" svgId="connector10pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector10pin"/>
          <p layer="copper1" svgId="connector10pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin 12" type="male" id="connector11">
      <description>GT_SO</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector11pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector11terminal" layer="schematic" svgId="connector11pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector11pin"/>
          <p layer="copper1" svgId="connector11pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin 13" type="male" id="connector12">
      <description>GT_SCL</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector12pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector12terminal" layer="schematic" svgId="connector12pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector12pin"/>
          <p layer="copper1" svgId="connector12pin"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin 14" type="male" id="connector13">
      <description>GND</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector13pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector13terminal" layer="schematic" svgId="connector13pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector13pin"/>
          <p layer="copper1" svgId="connector13pin"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <buses>
    <bus id="GND">
      <nodeMember connectorId="connector0"/>
      <nodeMember connectorId="connector13"/>
    </bus>
    <bus id="LEDA">
      <nodeMember connectorId="connector7"/>
      <nodeMember connectorId="connector8"/>
    </bus>
  </buses>
  <url>https://www.az-delivery.de/en/products/1-77-zoll-spi-tft-display</url>
  <label>LCD</label>
</module>
