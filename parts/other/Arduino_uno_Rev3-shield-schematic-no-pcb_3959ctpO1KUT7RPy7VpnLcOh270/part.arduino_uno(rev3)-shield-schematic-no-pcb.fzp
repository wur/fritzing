<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module fritzingVersion="0.9.4" moduleId="arduino_uno(rev3)-shield-schematic-no-pcb" referenceFile="arduino_uno(rev3)-shield-schematic-no-pcb.fzp">
  <version>1</version>
  <date>Sat Apr 11 2020</date>
  <author>althaus (modified by vanepp Apr 2020)</author>
  <label>Mod</label>
  <description>The Arduino Uno is a microcontroller board based on the ATmega328 (datasheet). It has 14 digital input/output pins (of which 6 can be used as PWM outputs), 6 analog inputs, a 16 MHz ceramic resonator, a USB connection, a power jack, an ICSP header, and a reset button. It contains everything needed to support the microcontroller; simply connect it to a computer with a USB cable or power it with a AC-to-DC adapter or battery to get started.
The Uno differs from all preceding boards in that it does not use the FTDI USB-to-serial driver chip. Instead, it features the Atmega16U2 (Atmega8U2 up to version R2) programmed as a USB-to-serial converter.
Revision 2 of the Uno board has a resistor pulling the 8U2 HWB line to ground, making it easier to put into DFU mode.
Revision 3 of the board has the following new features:
1.0 pinout: added SDA and SCL pins that are near to the AREF pin and two other new pins placed near to the RESET pin, the IOREF that allow the shields to adapt to the voltage provided from the board. In future, shields will be compatible both with the board that use the AVR, which operate with 5V and with the Arduino Due that operate with 3.3V. The second one is a not connected pin, that is reserved for future purposes.
Stronger RESET circuit.
Atmega 16U2 replace the 8U2.
"Uno" means one in Italian and is named to mark the upcoming release of Arduino 1.0. The Uno and version 1.0 will be the reference versions of Arduino, moving forward. The Uno is the latest in a series of USB Arduino boards, and the reference model for the Arduino platform; for a comparison with previous versions, see the index of Arduino boards. Apr 2020: Create a shield friendly version with schematic with all the pins on one edge, both ICSP connectors and no pcb view. The idea is to be able to plug a shield on the the Arduino in breadboard and have it connect to the shield in schematic but not appear in pcb. On the way by bring the part up to graphic standards with the correct pin sequence.</description>
  <title>Arduino Uno (Rev3) - ICSP Shield (no pcb view)</title>
  <url>http://arduino.cc/en/Main/ArduinoBoardUno</url>
  <tags>
    <tag>rev3</tag>
    <tag>uno</tag>
    <tag>arduino</tag>
    <tag>arduino shield</tag>
    <tag>ATmega328</tag>
  </tags>
  <properties>
    <property name="family">microcontroller board (arduino)</property>
    <property name="part number"/>
    <property name="type">Arduino UNO (Rev3) - ICSP Shield schematic no pcb</property>
    <property name="layer"/>
  </properties>
  <views>
    <breadboardView>
      <layers image="breadboard/arduino_uno(rev3)-shield-schematic-no-pcb_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/arduino_uno(rev3)-shield-schematic-no-pcb_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/arduino_uno(rev3)-shield-schematic-no-pcb_1_pcb.svg">
        <layer layerId="copper1"/>
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
      </layers>
    </pcbView>
    <iconView>
      <layers image="breadboard/arduino_uno(rev3)-shield-schematic-no-pcb_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
  </views>
  <connectors>
    <connector id="connector0" type="female" name="N/C">
      <description>not connected</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" layer="schematic" terminalId="connector0terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector1" type="female" name="IOREF">
      <description>IOREF</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" layer="schematic" terminalId="connector1terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector2" type="female" name="RESET">
      <description>RESET</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" layer="schematic" terminalId="connector2terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector3" type="female" name="3V3">
      <description>3V3</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" layer="schematic" terminalId="connector3terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector4" type="female" name="5V">
      <description>5V</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" layer="schematic" terminalId="connector4terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector5" type="female" name="GND">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" layer="schematic" terminalId="connector5terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector6" type="female" name="GND">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector6pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector6pin" layer="schematic" terminalId="connector6terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector7" type="female" name="VIN">
      <description>VIN</description>
      <views>
        <breadboardView>
          <p svgId="connector7pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector7pin" layer="schematic" terminalId="connector7terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector8" type="female" name="A0">
      <description>A0</description>
      <views>
        <breadboardView>
          <p svgId="connector8pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector8pin" layer="schematic" terminalId="connector8terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector9" type="female" name="A1">
      <description>A1</description>
      <views>
        <breadboardView>
          <p svgId="connector9pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector9pin" layer="schematic" terminalId="connector9terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector10" type="female" name="A2">
      <description>A2</description>
      <views>
        <breadboardView>
          <p svgId="connector10pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector10pin" layer="schematic" terminalId="connector10terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector11" type="female" name="A3">
      <description>A3</description>
      <views>
        <breadboardView>
          <p svgId="connector11pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector11pin" layer="schematic" terminalId="connector11terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector12" type="female" name="A4/SDA">
      <description>A4/SDA</description>
      <views>
        <breadboardView>
          <p svgId="connector12pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector12pin" layer="schematic" terminalId="connector12terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector13" type="female" name="A5/SCL">
      <description>A5/SCL</description>
      <views>
        <breadboardView>
          <p svgId="connector13pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector13pin" layer="schematic" terminalId="connector13terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector14" type="female" name="D0/RX">
      <description>D0/RX</description>
      <views>
        <breadboardView>
          <p svgId="connector14pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector14pin" layer="schematic" terminalId="connector14terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector15" type="female" name="D1/TX">
      <description>D1/TX</description>
      <views>
        <breadboardView>
          <p svgId="connector15pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector15pin" layer="schematic" terminalId="connector15terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector16" type="female" name="D2">
      <description>D2</description>
      <views>
        <breadboardView>
          <p svgId="connector16pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector16pin" layer="schematic" terminalId="connector16terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector17" type="female" name="D3 PWM">
      <description>D3 PWM</description>
      <views>
        <breadboardView>
          <p svgId="connector17pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector17pin" layer="schematic" terminalId="connector17terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector18" type="female" name="D4">
      <description>D4</description>
      <views>
        <breadboardView>
          <p svgId="connector18pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector18pin" layer="schematic" terminalId="connector18terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector19" type="female" name="D5 PWM">
      <description>D5 PWM</description>
      <views>
        <breadboardView>
          <p svgId="connector19pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector19pin" layer="schematic" terminalId="connector19terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector20" type="female" name="D6 PWM">
      <description>D6 PWM</description>
      <views>
        <breadboardView>
          <p svgId="connector20pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector20pin" layer="schematic" terminalId="connector20terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector21" type="female" name="D7">
      <description>D7</description>
      <views>
        <breadboardView>
          <p svgId="connector21pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector21pin" layer="schematic" terminalId="connector21terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector22" type="female" name="D8">
      <description>D8</description>
      <views>
        <breadboardView>
          <p svgId="connector22pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector22pin" layer="schematic" terminalId="connector22terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector23" type="female" name="D9 PWM">
      <description>D9 PWM</description>
      <views>
        <breadboardView>
          <p svgId="connector23pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector23pin" layer="schematic" terminalId="connector23terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector24" type="female" name="D10 PWM/SS">
      <description>D10 PWM/SS</description>
      <views>
        <breadboardView>
          <p svgId="connector24pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector24pin" layer="schematic" terminalId="connector24terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector25" type="female" name="D11 PWM/MOSI">
      <description>D11 PWM/MOSI</description>
      <views>
        <breadboardView>
          <p svgId="connector25pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector25pin" layer="schematic" terminalId="connector25terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector26" type="female" name="D12/MISO">
      <description>D12/MISO</description>
      <views>
        <breadboardView>
          <p svgId="connector26pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector26pin" layer="schematic" terminalId="connector26terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector27" type="female" name="D13/SCK">
      <description>D13/SCK</description>
      <views>
        <breadboardView>
          <p svgId="connector27pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector27pin" layer="schematic" terminalId="connector27terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector28" type="female" name="GND">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector28pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector28pin" layer="schematic" terminalId="connector28terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector29" type="female" name="AREF">
      <description>AREF</description>
      <views>
        <breadboardView>
          <p svgId="connector29pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector29pin" layer="schematic" terminalId="connector29terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector30" type="female" name="A4/SDA">
      <description>A4/SDA</description>
      <views>
        <breadboardView>
          <p svgId="connector30pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector30pin" layer="schematic" terminalId="connector30terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector31" type="female" name="A5/SCL">
      <description>A5/SCL</description>
      <views>
        <breadboardView>
          <p svgId="connector31pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector31pin" layer="schematic" terminalId="connector31terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector32" type="female" name="RESET">
      <description>RESET</description>
      <views>
        <breadboardView>
          <p svgId="connector32pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector32pin" layer="schematic" terminalId="connector32terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector33" type="female" name="GND">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector33pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector33pin" layer="schematic" terminalId="connector33terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector34" type="female" name="ICSP SCK">
      <description>ICSP SCK</description>
      <views>
        <breadboardView>
          <p svgId="connector34pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector34pin" layer="schematic" terminalId="connector34terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector35" type="female" name="ICSP MOSI">
      <description>ICSP MOSI</description>
      <views>
        <breadboardView>
          <p svgId="connector35pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector35pin" layer="schematic" terminalId="connector35terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector36" type="female" name="ICSP MISO">
      <description>ICSP MISO</description>
      <views>
        <breadboardView>
          <p svgId="connector36pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector36pin" layer="schematic" terminalId="connector36terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector37" type="female" name="5V">
      <description>5V</description>
      <views>
        <breadboardView>
          <p svgId="connector37pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector37pin" layer="schematic" terminalId="connector37terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector38" type="female" name="5V">
      <description>5V</description>
      <views>
        <breadboardView>
          <p svgId="connector38pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector38pin" layer="schematic" terminalId="connector38terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector39" type="female" name="ICSP2 MISO">
      <description>ICSP2 MISO</description>
      <views>
        <breadboardView>
          <p svgId="connector39pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector39pin" layer="schematic" terminalId="connector39terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector40" type="female" name="ICSP2 MOSI">
      <description>ICSP2 MOSI</description>
      <views>
        <breadboardView>
          <p svgId="connector40pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector40pin" layer="schematic" terminalId="connector40terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector41" type="female" name="ICSP2 SCK">
      <description>ICSP2 SCK</description>
      <views>
        <breadboardView>
          <p svgId="connector41pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector41pin" layer="schematic" terminalId="connector41terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector42" type="female" name="GND">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector42pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector42pin" layer="schematic" terminalId="connector42terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector43" type="female" name="RESET2">
      <description>RESET2</description>
      <views>
        <breadboardView>
          <p svgId="connector43pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector43pin" layer="schematic" terminalId="connector43terminal"/>
        </schematicView>
      </views>
    </connector>
  </connectors>
  <buses>
    <bus id="5V">
      <nodeMember connectorId="connector4"/>
      <nodeMember connectorId="connector37"/>
      <nodeMember connectorId="connector38"/>
    </bus>
    <bus id="GND">
      <nodeMember connectorId="connector5"/>
      <nodeMember connectorId="connector6"/>
      <nodeMember connectorId="connector28"/>
      <nodeMember connectorId="connector33"/>
      <nodeMember connectorId="connector42"/>
    </bus>
    <bus id="MOSI">
      <nodeMember connectorId="connector25"/>
      <nodeMember connectorId="connector35"/>
    </bus>
    <bus id="MISO">
      <nodeMember connectorId="connector26"/>
      <nodeMember connectorId="connector36"/>
    </bus>
    <bus id="SCK">
      <nodeMember connectorId="connector27"/>
      <nodeMember connectorId="connector34"/>
    </bus>
    <bus id="A4/SDA">
      <nodeMember connectorId="connector12"/>
      <nodeMember connectorId="connector30"/>
    </bus>
    <bus id="A5/SCL">
      <nodeMember connectorId="connector13"/>
      <nodeMember connectorId="connector31"/>
    </bus>
    <bus id="RESET">
      <nodeMember connectorId="connector2"/>
      <nodeMember connectorId="connector32"/>
    </bus>
  </buses>
</module>
