<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module referenceFile="TA8210_1.fzp" moduleId="TA8210_1" fritzingVersion="0.9.4">
  <version>4</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>TA8210</title>
  <label>Mod</label>
  <date>Sun Sep 20 2020</date>
  <tags>
    <tag>TA8210</tag>
    <tag>20w audio power amplifier</tag>
  </tags>
  <properties>
    <property name="Pins">17</property>
    <property name="hole size">0.6mm,0.508mm</property>
    <property name="family">TA8210</property>
    <property name="package">THT</property>
    <property name="part number"/>
    <property name="variant">variant 1</property>
    <property name="Position"/>
    <property name="layer"/>
    <property name="Pin Spacing">0.07874in (2mm)</property>
    <property name="Row">single</property>
  </properties>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;20W BTL x2ch Audio Power amplifier&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/TA8210_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/TA8210_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/TA8210_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/TA8210_1_pcb.svg">
        <layer layerId="copper0"/>
        <layer layerId="silkscreen"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector type="male" name="pin1" id="connector0">
      <description>MUTE SW</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" layer="schematic" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin2" id="connector1">
      <description>IN1</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" layer="schematic" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin3" id="connector2">
      <description>NF1</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" layer="schematic" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin4" id="connector3">
      <description>STANDBY SW</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" layer="schematic" terminalId="connector3terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin5" id="connector4">
      <description>PRE GND</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" layer="schematic" terminalId="connector4terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin6" id="connector5">
      <description>NF2</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" layer="schematic" terminalId="connector5terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper0"/>
          <p svgId="connector5pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin7" id="connector6">
      <description>IN2</description>
      <views>
        <breadboardView>
          <p svgId="connector6pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector6pin" layer="schematic" terminalId="connector6terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector6pin" layer="copper0"/>
          <p svgId="connector6pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin8" id="connector7">
      <description>RIPPLE</description>
      <views>
        <breadboardView>
          <p svgId="connector7pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector7pin" layer="schematic" terminalId="connector7terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector7pin" layer="copper0"/>
          <p svgId="connector7pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin9" id="connector8">
      <description>PRE VCC</description>
      <views>
        <breadboardView>
          <p svgId="connector8pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector8pin" layer="schematic" terminalId="connector8terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector8pin" layer="copper0"/>
          <p svgId="connector8pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin10" id="connector9">
      <description>VCC1</description>
      <views>
        <breadboardView>
          <p svgId="connector9pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector9pin" layer="schematic" terminalId="connector9terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector9pin" layer="copper0"/>
          <p svgId="connector9pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin11" id="connector10">
      <description>OUT3</description>
      <views>
        <breadboardView>
          <p svgId="connector10pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector10pin" layer="schematic" terminalId="connector10terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector10pin" layer="copper0"/>
          <p svgId="connector10pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin12" id="connector11">
      <description>OUT4</description>
      <views>
        <breadboardView>
          <p svgId="connector11pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector11pin" layer="schematic" terminalId="connector11terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector11pin" layer="copper0"/>
          <p svgId="connector11pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin13" id="connector12">
      <description>GND2</description>
      <views>
        <breadboardView>
          <p svgId="connector12pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector12pin" layer="schematic" terminalId="connector12terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector12pin" layer="copper0"/>
          <p svgId="connector12pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin14" id="connector13">
      <description>GND1</description>
      <views>
        <breadboardView>
          <p svgId="connector13pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector13pin" layer="schematic" terminalId="connector13terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector13pin" layer="copper0"/>
          <p svgId="connector13pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin15" id="connector14">
      <description>OUT1</description>
      <views>
        <breadboardView>
          <p svgId="connector14pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector14pin" layer="schematic" terminalId="connector14terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector14pin" layer="copper0"/>
          <p svgId="connector14pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin16" id="connector15">
      <description>OUT2</description>
      <views>
        <breadboardView>
          <p svgId="connector15pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector15pin" layer="schematic" terminalId="connector15terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector15pin" layer="copper0"/>
          <p svgId="connector15pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin17" id="connector16">
      <description>VCC2</description>
      <views>
        <breadboardView>
          <p svgId="connector16pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector16pin" layer="schematic" terminalId="connector16terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector16pin" layer="copper0"/>
          <p svgId="connector16pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
</module>
