<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module referenceFile="ATtiny3216-soic20-footprint_1.fzp" moduleId="ATtiny3216-soic20-footprint_1" fritzingVersion="0.9.6">
  <version>1</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>ATtiny3216 soic20 footprint</title>
  <label>U</label>
  <date>Sun May 2 2021</date>
  <tags>
    <tag>SOIC20</tag>
    <tag>fritzing user</tag>
  </tags>
  <properties>
    <property name="family">ATtiny3216 soic20 footprint</property>
    <property name="package">SOIC20 [SMD]</property>
    <property name="pins">20</property>
    <property name="chip label">U</property>
    <property name="editable pin labels">false</property>
    <property name="part number"/>
    <property name="layer"/>
    <property name="variant">variant 1</property>
  </properties>
  <description>ATtiny3216 soic20 footprint. (only the footprint not the complete part!</description>
  <views>
    <iconView>
      <layers image="breadboard/ATtiny3216-soic20-footprint_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/ATtiny3216-soic20-footprint_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/ATtiny3216-soic20-footprint_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/ATtiny3216-soic20-footprint_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector name="pin1" id="connector0" type="male">
      <description>pin 1</description>
      <views>
        <breadboardView>
          <p terminalId="connector0terminal" layer="breadboard" svgId="connector0pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector0terminal" layer="schematic" svgId="connector0pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector0pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin2" id="connector1" type="male">
      <description>pin 2</description>
      <views>
        <breadboardView>
          <p terminalId="connector1terminal" layer="breadboard" svgId="connector1pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector1terminal" layer="schematic" svgId="connector1pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector1pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin3" id="connector2" type="male">
      <description>pin 3</description>
      <views>
        <breadboardView>
          <p terminalId="connector2terminal" layer="breadboard" svgId="connector2pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector2terminal" layer="schematic" svgId="connector2pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector2pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin4" id="connector3" type="male">
      <description>pin 4</description>
      <views>
        <breadboardView>
          <p terminalId="connector3terminal" layer="breadboard" svgId="connector3pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector3terminal" layer="schematic" svgId="connector3pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector3pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin5" id="connector4" type="male">
      <description>pin 5</description>
      <views>
        <breadboardView>
          <p terminalId="connector4terminal" layer="breadboard" svgId="connector4pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector4terminal" layer="schematic" svgId="connector4pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector4pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin6" id="connector5" type="male">
      <description>pin 6</description>
      <views>
        <breadboardView>
          <p terminalId="connector5terminal" layer="breadboard" svgId="connector5pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector5terminal" layer="schematic" svgId="connector5pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector5pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin7" id="connector6" type="male">
      <description>pin 7</description>
      <views>
        <breadboardView>
          <p terminalId="connector6terminal" layer="breadboard" svgId="connector6pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector6terminal" layer="schematic" svgId="connector6pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector6pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin8" id="connector7" type="male">
      <description>pin 8</description>
      <views>
        <breadboardView>
          <p terminalId="connector7terminal" layer="breadboard" svgId="connector7pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector7terminal" layer="schematic" svgId="connector7pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector7pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin9" id="connector8" type="male">
      <description>pin 9</description>
      <views>
        <breadboardView>
          <p terminalId="connector8terminal" layer="breadboard" svgId="connector8pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector8terminal" layer="schematic" svgId="connector8pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector8pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin10" id="connector9" type="male">
      <description>pin 10</description>
      <views>
        <breadboardView>
          <p terminalId="connector9terminal" layer="breadboard" svgId="connector9pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector9terminal" layer="schematic" svgId="connector9pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector9pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin11" id="connector10" type="male">
      <description>pin 11</description>
      <views>
        <breadboardView>
          <p terminalId="connector10terminal" layer="breadboard" svgId="connector10pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector10terminal" layer="schematic" svgId="connector10pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector10pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin12" id="connector11" type="male">
      <description>pin 12</description>
      <views>
        <breadboardView>
          <p terminalId="connector11terminal" layer="breadboard" svgId="connector11pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector11terminal" layer="schematic" svgId="connector11pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector11pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin13" id="connector12" type="male">
      <description>pin 13</description>
      <views>
        <breadboardView>
          <p terminalId="connector12terminal" layer="breadboard" svgId="connector12pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector12terminal" layer="schematic" svgId="connector12pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector12pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin14" id="connector13" type="male">
      <description>pin 14</description>
      <views>
        <breadboardView>
          <p terminalId="connector13terminal" layer="breadboard" svgId="connector13pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector13terminal" layer="schematic" svgId="connector13pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector13pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin15" id="connector14" type="male">
      <description>pin 15</description>
      <views>
        <breadboardView>
          <p terminalId="connector14terminal" layer="breadboard" svgId="connector14pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector14terminal" layer="schematic" svgId="connector14pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector14pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin16" id="connector15" type="male">
      <description>pin 16</description>
      <views>
        <breadboardView>
          <p terminalId="connector15terminal" layer="breadboard" svgId="connector15pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector15terminal" layer="schematic" svgId="connector15pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector15pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin17" id="connector16" type="male">
      <description>pin 17</description>
      <views>
        <breadboardView>
          <p terminalId="connector16terminal" layer="breadboard" svgId="connector16pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector16terminal" layer="schematic" svgId="connector16pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector16pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin18" id="connector17" type="male">
      <description>pin 18</description>
      <views>
        <breadboardView>
          <p terminalId="connector17terminal" layer="breadboard" svgId="connector17pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector17terminal" layer="schematic" svgId="connector17pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector17pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin19" id="connector18" type="male">
      <description>pin 19</description>
      <views>
        <breadboardView>
          <p terminalId="connector18terminal" layer="breadboard" svgId="connector18pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector18terminal" layer="schematic" svgId="connector18pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector18pad"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin20" id="connector19" type="male">
      <description>pin 20</description>
      <views>
        <breadboardView>
          <p terminalId="connector19terminal" layer="breadboard" svgId="connector19pin"/>
        </breadboardView>
        <schematicView>
          <p terminalId="connector19terminal" layer="schematic" svgId="connector19pin"/>
        </schematicView>
        <pcbView>
          <p layer="copper1" svgId="connector19pad"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
</module>
