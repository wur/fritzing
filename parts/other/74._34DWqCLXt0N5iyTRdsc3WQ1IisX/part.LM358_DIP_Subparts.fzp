<?xml version='1.0' encoding='UTF-8'?>
<module fritzingVersion="0.9.3b" moduleId="LM358_DIP_Subparts" referenceFile="LM358_DIP_Subparts.fzp">
 <version>5</version>
 <author>arnab.blue</author>
 <title>LM 358 Dual Op-Amp Subparts</title>
 <label>U</label>
 <date>Wed Sep 18 2019</date>
 <tags>
  <tag>Op-Amp,LM358</tag>
  <tag>IC</tag>
  <tag>DIP</tag>
  <tag>dual op amp</tag>
 </tags>
 <properties>
  <property name="family">op-amp ic</property>
  <property name="package">DIP8 [THT]</property>
  <property name="layer"></property>
  <property name="part number"></property>
  <property name="variant">LM358, LM393</property>
 </properties>
 <description>The classic LM 358 Dual Operational Amplifier (Op-Amp).  The analog swiss-army knife.</description>
 <views>
  <iconView>
   <layers image="icon/LM358_DIP_Subparts_icon.svg">
    <layer layerId="icon"/>
   </layers>
  </iconView>
  <breadboardView>
   <layers image="breadboard/LM358_DIP_Subparts_breadboard.svg">
    <layer layerId="breadboard"/>
   </layers>
  </breadboardView>
  <schematicView>
   <layers image="schematic/LM358_DIP_Subparts_schematic.svg">
    <layer layerId="schematic"/>
   </layers>
  </schematicView>
  <pcbView>
   <layers image="pcb/LM358_DIP_Subparts_pcb.svg">
    <layer layerId="copper0"/>
    <layer layerId="soldermask"/>
    <layer layerId="silkscreen"/>
    <layer layerId="copper1"/>
   </layers>
  </pcbView>
 </views>
 <connectors>
  <connector name="Pin 4" id="connector3" type="male">
   <description>Ground</description>
   <views>
    <breadboardView>
     <p svgId="connector3pin" layer="breadboard" terminalId="connector3terminal"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector3pin" layer="schematic" terminalId="connector3terminal"/>
    </schematicView>
    <pcbView>
     <p svgId="connector3pin" layer="copper0"/>
     <p svgId="connector3pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="Pin 5" id="connector4" type="male">
   <description>Non-Inverting Input B</description>
   <views>
    <breadboardView>
     <p svgId="connector4pin" layer="breadboard" terminalId="connector4terminal"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector4pin" layer="schematic" terminalId="connector4terminal"/>
    </schematicView>
    <pcbView>
     <p svgId="connector4pin" layer="copper0"/>
     <p svgId="connector4pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="Pin 6" id="connector5" type="male">
   <description>Inverting Input B</description>
   <views>
    <breadboardView>
     <p svgId="connector5pin" layer="breadboard" terminalId="connector5terminal"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector5pin" layer="schematic" terminalId="connector5terminal"/>
    </schematicView>
    <pcbView>
     <p svgId="connector5pin" layer="copper0"/>
     <p svgId="connector5pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="Pin 7" id="connector6" type="male">
   <description>Output B</description>
   <views>
    <breadboardView>
     <p svgId="connector6pin" layer="breadboard" terminalId="connector6terminal"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector6pin" layer="schematic" terminalId="connector6terminal"/>
    </schematicView>
    <pcbView>
     <p svgId="connector6pin" layer="copper0"/>
     <p svgId="connector6pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="Pin 8" id="connector7" type="male">
   <description>V+</description>
   <views>
    <breadboardView>
     <p svgId="connector7pin" layer="breadboard" terminalId="connector7terminal"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector7pin" layer="schematic" terminalId="connector7terminal"/>
    </schematicView>
    <pcbView>
     <p svgId="connector7pin" layer="copper0"/>
     <p svgId="connector7pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="Pin 1" id="connector0" type="male">
   <description>Output A</description>
   <views>
    <breadboardView>
     <p svgId="connector0pin" layer="breadboard" terminalId="connector0terminal"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector0pin" layer="schematic" terminalId="connector0terminal"/>
    </schematicView>
    <pcbView>
     <p svgId="connector0pin" layer="copper0"/>
     <p svgId="connector0pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="Pin 2" id="connector1" type="male">
   <description>Inverting Input A</description>
   <views>
    <breadboardView>
     <p svgId="connector1pin" layer="breadboard" terminalId="connector1terminal"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector1pin" layer="schematic" terminalId="connector1terminal"/>
    </schematicView>
    <pcbView>
     <p svgId="connector1pin" layer="copper0"/>
     <p svgId="connector1pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="Pin 3" id="connector2" type="male">
   <description>Non-Inverting Input A</description>
   <views>
    <breadboardView>
     <p svgId="connector2pin" layer="breadboard" terminalId="connector2terminal"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector2pin" layer="schematic" terminalId="connector2terminal"/>
    </schematicView>
    <pcbView>
     <p svgId="connector2pin" layer="copper0"/>
     <p svgId="connector2pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
 </connectors>
 <schematic-subparts>
  <subpart id="subpart1" label="1">
   <connectors>
    <connector id="connector0"/>
    <connector id="connector1"/>
    <connector id="connector2"/>
    <connector id="connector3"/>
    <connector id="connector7"/>
   </connectors>
  </subpart>
  <subpart id="subpart2" label="2">
   <connectors>
    <connector id="connector4"/>
    <connector id="connector5"/>
    <connector id="connector6"/>
   </connectors>
  </subpart>
 </schematic-subparts>
</module>
