<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module moduleId="dfr-sen0308_1" referenceFile="dfr-sen0308_1.fzp" fritzingVersion="0.9.9">
  <version>4</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>DFR sen0308</title>
  <label>M</label>
  <date>Sat Feb 26 2022</date>
  <tags>
    <tag>contrib</tag>
    <tag>DFR sen0308</tag>
    <tag>Soil moisture sensor</tag>
    <tag>fritzing user</tag>
    <tag>sensor</tag>
  </tags>
  <properties>
    <property name="variant">variant 1</property>
    <property name="family">DFR sen0308</property>
    <property name="editable pin labels">false</property>
    <property name="part number"/>
    <property name="pins">4</property>
    <property name="layer"/>
    <property name="package">THT</property>
  </properties>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;This is a new type of waterproof soil moisture sensor introduced by DFRobot. Compared with the previous version of the soil moisture sensor, it has increased waterproof performance, optimized corrosion resistance, increased plate length and optimized circuit performance. Compared with the resistive sensor, the capacitive soil moisture sensor solves the problem that the resistive sensor is easily corroded, and can be inserted into the soil for a long time without being corroded. The sensor has increased waterproof performance, and the sensor can still be used normally after being immersed in water.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/dfr-sen0308_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/dfr-sen0308_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/dfr-sen0308_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/dfr-sen0308_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" name="pin1" type="male">
      <description>GND (BLK)</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector0pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector0pin" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector0pin"/>
          <p layer="copper1" svgId="connector0pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" name="pin2" type="male">
      <description>SIG (YEL)</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector1pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector1pin" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector1pin"/>
          <p layer="copper1" svgId="connector1pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" name="pin3" type="male">
      <description>GND (BLK)</description>
      <views> terminalId="connector2terminal"
        <breadboardView>
          <p layer="breadboard" svgId="connector2pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector2pin" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector2pin"/>
          <p layer="copper1" svgId="connector2pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" name="pin4" type="male">
      <description>VCC (RED)</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector3pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector3pin" terminalId="connector3terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector3pin"/>
          <p layer="copper1" svgId="connector3pin"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <buses>
    <bus id="GND">
      <nodeMember connectorId="connector0"/>
      <nodeMember connectorId="connector2"/>
    </bus>
  </buses>
  <url>https://wiki.dfrobot.com/Waterproof_Capacitive_Soil_Moisture_Sensor_SKU_SEN0308</url>
</module>
