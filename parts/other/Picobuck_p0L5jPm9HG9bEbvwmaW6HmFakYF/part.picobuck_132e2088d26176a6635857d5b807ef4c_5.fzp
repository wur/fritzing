<?xml version='1.0' encoding='UTF-8'?>
<module referenceFile="generic_ic_dip_8_300mil.fzp" moduleId="picobuck_132e2088d26176a6635857d5b807ef4c_5">
 <version>4</version>
 <author>Haley Foard</author>
 <title>Picobuck </title>
 <label>IC</label>
 <date>Thu Jun 25 2020</date>
 <tags>
  <tag>DIP</tag>
  <tag>fritzing core</tag>
 </tags>
 <properties>
  <property name="family">Generic IC</property>
  <property name="variant">variant 1</property>
  <property name="pins">8</property>
  <property name="pin spacing">300mil</property>
  <property name="editable pin labels">false</property>
  <property name="chip label">IC</property>
  <property name="part number"/>
  <property name="layer"/>
  <property name="hole size"/>
  <property name="package">DIP (Dual Inline) [THT]</property>
 </properties>
 <taxonomy>part.dip.8.pins</taxonomy>
 <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
&lt;html>&lt;head>&lt;meta name="qrichtext" content="1" />&lt;style type="text/css">
p, li { white-space: pre-wrap; }
&lt;/style>&lt;/head>&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:7.8pt; font-weight:400; font-style:normal;">
&lt;p style=" margin-top:0px; margin-bottom:10px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;">&lt;span style=" font-family:'Helvetica Neue,Helvetica,Arial,sans-serif'; font-size:14px; color:#333333; background-color:#ffffff;">The PicoBuck LED Driver is an economical and easy to use driver that will allow you to control and blend three different LEDs on three different channels. By default, each channel is driven at 330mA; that current can be reduced by either presenting an analog voltage or a PWM signal to the board. Version 12 of the board adds a solderable jumper that can be closed to increase the maximum current to 660mA. The new voltage regulator also increased the voltage rating on the various components on the board, allowing it to be used up to the full 36V rating of the AL8805 part.&lt;/span>&lt;/p>
&lt;p style=" margin-top:0px; margin-bottom:10px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;">&lt;span style=" font-family:'Helvetica Neue,Helvetica,Arial,sans-serif'; font-size:14px; color:#333333; background-color:#ffffff;">Three signal inputs are provided for dimming control. You can use the PWM signal from an Arduino or your favorite microcontroller to dim each channel individually, or you can tie them all to the same PWM for simultaneous dimming. Dimming can be done by an analog voltage (20%-100% of max current by varying voltage from .5V-2.5V) or by PWM (so long as PWM minimum voltage is less than .4V and maximum voltage is more than 2.4V) for a full 0-100% range. A small jumper is provided for each channel to allow you to increase the drive strength from 330mA to 660mA. Two mounting holes for 4-40 or M3 screws are provided on either side of the board. They are perforated so they can be easily snapped off with a pair of pliers, if a smaller footprint is desired.&lt;/span>&lt;/p>
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">&lt;br />&lt;/p>&lt;/body>&lt;/html></description>
 <views>
  <iconView>
   <layers image="icon/picobuck_fe6010710c98ce2acebc1fd0df4062a9_5_icon.svg">
    <layer layerId="icon"/>
   </layers>
  </iconView>
  <breadboardView>
   <layers image="breadboard/picobuck_fe6010710c98ce2acebc1fd0df4062a9_5_breadboard.svg">
    <layer layerId="breadboard"/>
   </layers>
  </breadboardView>
  <schematicView>
   <layers image="schematic/picobuck_fe6010710c98ce2acebc1fd0df4062a9_5_schematic.svg">
    <layer layerId="schematic"/>
   </layers>
  </schematicView>
  <pcbView>
   <layers image="pcb/picobuck_fe6010710c98ce2acebc1fd0df4062a9_5_pcb.svg">
    <layer layerId="silkscreen"/>
    <layer layerId="copper0"/>
    <layer layerId="copper1"/>
   </layers>
  </pcbView>
 </views>
 <connectors>
  <connector id="connector0" type="male" name="+ OUT 1">
   <description/>
   <views>
    <breadboardView>
     <p terminalId="connector0terminal" svgId="connector0pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector0terminal" svgId="connector0pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector0pin" layer="copper0"/>
     <p svgId="connector0pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector1" type="male" name="- OUT 1">
   <description>negative</description>
   <views>
    <breadboardView>
     <p terminalId="connector1terminal" svgId="connector1pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector1terminal" svgId="connector1pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector1pin" layer="copper0"/>
     <p svgId="connector1pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector2" type="male" name="+ OUT 2">
   <description>Positive</description>
   <views>
    <breadboardView>
     <p terminalId="connector2terminal" svgId="connector2pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector2terminal" svgId="connector2pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector2pin" layer="copper0"/>
     <p svgId="connector2pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector3" type="male" name="- OUT 2">
   <description>pin 4</description>
   <views>
    <breadboardView>
     <p terminalId="connector3terminal" svgId="connector3pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector3terminal" svgId="connector3pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector3pin" layer="copper0"/>
     <p svgId="connector3pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector4" type="male" name="+ OUT 3">
   <description>pin 5</description>
   <views>
    <breadboardView>
     <p terminalId="connector4terminal" svgId="connector4pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector4terminal" svgId="connector4pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector4pin" layer="copper0"/>
     <p svgId="connector4pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector5" type="male" name="- OUT 3">
   <description>NEgative</description>
   <views>
    <breadboardView>
     <p terminalId="connector5terminal" svgId="connector5pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector5terminal" svgId="connector5pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector5pin" layer="copper0"/>
     <p svgId="connector5pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector6" type="male" name="IN 1">
   <description>IN</description>
   <views>
    <breadboardView>
     <p terminalId="connector6terminal" svgId="connector6pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector6terminal" svgId="connector6pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector6pin" layer="copper0"/>
     <p svgId="connector6pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector7" type="male" name="IN 2">
   <description>pin 8</description>
   <views>
    <breadboardView>
     <p terminalId="connector7terminal" svgId="connector7pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p terminalId="connector7terminal" svgId="connector7pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p terminalId="connector7terminal" svgId="connector7pin" layer="copper0"/>
     <p svgId="connector7pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector8" type="male" name="IN 3">
   <description>pin 8</description>
   <views>
    <breadboardView>
     <p svgId="connector8pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector8pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector8pin" layer="copper0"/>
     <p svgId="connector8pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector9" type="male" name="GND">
   <description>Ground</description>
   <views>
    <breadboardView>
     <p svgId="connector9pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector9pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector9pin" layer="copper0"/>
     <p svgId="connector9pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector10" type="male" name="- VIN">
   <description>pin 10</description>
   <views>
    <breadboardView>
     <p svgId="connector10pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector10pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector10pin" layer="copper0"/>
     <p svgId="connector10pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector11" type="male" name="+ VIN">
   <description>pin 11</description>
   <views>
    <breadboardView>
     <p svgId="connector11pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector11pin" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector11pin" layer="copper0"/>
     <p svgId="connector11pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
 </connectors>
</module>
