<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module moduleId="voltammeter_1" referenceFile="voltammeter_1.fzp" fritzingVersion="0.9.6">
  <version>1</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>volt-ammeter</title>
  <label>M</label>
  <date>Sat Apr 17 2021</date>
  <tags>
    <tag>contrib</tag>
    <tag>voltmeter</tag>
    <tag>ammeter</tag>
    <tag>fritzing user</tag>
  </tags>
  <properties>
    <property name="variant">variant 1</property>
    <property name="family">volt-ammeter</property>
    <property name="editable pin labels">false</property>
    <property name="part number"/>
    <property name="layer"/>
    <property name="package">THT</property>
  </properties>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;Volt-ammeter 0-100V 0-10A panel mount.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/voltammeter_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/voltammeter_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/voltammeter_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/voltammeter_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" name="-PWR" type="male">
      <description>Input power ground (one end of the current shunt) </description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector0pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector0pin" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector0pin"/>
          <p layer="copper1" svgId="connector0pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" name="-LOAD" type="male">
      <description>Load end of the current shunt to the load ground</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector1pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector1pin" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector1pin"/>
          <p layer="copper1" svgId="connector1pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" name="LOAD V+" type="male">
      <description>Load voltage sense to the + terminal of the load</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector2pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector2pin" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector2pin"/>
          <p layer="copper1" svgId="connector2pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" name="Meter +" type="male">
      <description>VCC for the meter</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector3pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector3pin" terminalId="connector3terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector3pin"/>
          <p layer="copper1" svgId="connector3pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector4" name="Meter -" type="male">
      <description>ground for the meter (connected to load ground)</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector4pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector4pin" terminalId="connector4terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector4pin"/>
          <p layer="copper1" svgId="connector4pin"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <buses>
    <bus id="GND">
      <nodeMember connectorId="connector0"/>
      <nodeMember connectorId="connector4"/>
    </bus>
  </buses>
</module>
