<?xml version='1.0' encoding='UTF-8'?>
<module fritzingVersion="0.12.34" moduleId="prefix0000_9fd29b632643db4a7b90632b89537aad_3" referenceFile="pot_trimmer_12mm.fzp">
 <version>4</version>
 <author>Stefan Hermann</author>
 <title>Trimmer Potentiometer</title>
 <label>R</label>
 <date>Mon Dec 27 2021</date>
 <tags>
  <tag>potentiometer</tag>
  <tag>poti</tag>
  <tag>adjustable</tag>
  <tag>resistor</tag>
  <tag>fritzing core</tag>
 </tags>
 <properties>
  <property name="family">potentiometer</property>
  <property name="type">Trimmer Potentiometer</property>
  <property name="Maximum Resistance" showInLabel="yes">10kΩ</property>
  <property name="Track">Linear</property>
  <property name="power" showInLabel="yes"></property>
  <property name="Size">Trimmer - 12mm</property>
  <property name="package">THT</property>
  <property name="layer"></property>
  <property name="part number"></property>
  <property name="variant">variant 1</property>
 </properties>
 <taxonomy>discreteParts.resistors.adjustable.potentiometer</taxonomy>
 <description>A standard adjustable resistor (potentiometer).</description>
 <spice>
  <line>R{instanceTitle}A {net connector0} {net connector1} {{knob status}/100*{maximum resistance}}</line>
  <line>R{instanceTitle}B {net connector2} {net connector1} {{maximum resistance}-{knob status}/100*{maximum resistance}}</line>
 </spice>
 <views>
  <iconView>
   <layers image="icon/prefix0000_9fd29b632643db4a7b90632b89537aad_3_icon.svg">
    <layer layerId="icon"/>
   </layers>
  </iconView>
  <breadboardView>
   <layers image="breadboard/prefix0000_9fd29b632643db4a7b90632b89537aad_3_breadboard.svg">
    <layer layerId="breadboard"/>
   </layers>
  </breadboardView>
  <schematicView fliphorizontal="true" flipvertical="true">
   <layers image="schematic/prefix0000_9fd29b632643db4a7b90632b89537aad_3_schematic.svg">
    <layer layerId="schematic"/>
   </layers>
  </schematicView>
  <pcbView>
   <layers image="pcb/prefix0000_9fd29b632643db4a7b90632b89537aad_3_pcb.svg">
    <layer layerId="silkscreen"/>
    <layer layerId="copper0"/>
    <layer layerId="copper1"/>
   </layers>
  </pcbView>
 </views>
 <connectors>
  <connector id="connector0" name="leg1" type="male">
   <description>leg1</description>
   <views>
    <breadboardView>
     <p svgId="connector0pin" layer="breadboard" terminalId="connector0terminal"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector0pin" layer="schematic" terminalId="connector0terminal"/>
    </schematicView>
    <pcbView>
     <p svgId="connector0pad" layer="copper0"/>
     <p svgId="connector0pad" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector1" name="wiper" type="male">
   <description>wiper</description>
   <views>
    <breadboardView>
     <p svgId="connector1pin" layer="breadboard" terminalId="connector1terminal"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector1pin" layer="schematic" terminalId="connector1terminal"/>
    </schematicView>
    <pcbView>
     <p svgId="connector1pad" layer="copper0"/>
     <p svgId="connector1pad" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector id="connector2" name="leg2" type="male">
   <description>leg2</description>
   <views>
    <breadboardView>
     <p svgId="connector2pin" layer="breadboard" terminalId="connector2terminal"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector2pin" layer="schematic" terminalId="connector2terminal"/>
    </schematicView>
    <pcbView>
     <p svgId="connector2pad" layer="copper0"/>
     <p svgId="connector2pad" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
 </connectors>
</module>
