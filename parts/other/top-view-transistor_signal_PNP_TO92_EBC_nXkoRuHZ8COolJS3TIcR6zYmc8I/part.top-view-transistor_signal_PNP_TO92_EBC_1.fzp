<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module fritzingVersion="0.9.9" moduleId="top-view-transistor_signal_PNP_TO92_EBC_1">
  <version>1</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>Top view PNP Transistor EBC</title>
  <label>Q</label>
  <date>Tue Oct 5 2021</date>
  <tags>
    <tag>PNP</tag>
    <tag>transistor</tag>
    <tag>signal transistor</tag>
    <tag>amplifier</tag>
    <tag>top view</tag>
    <tag>fritzing user</tag>
  </tags>
  <properties>
    <property name="package">TO92 [THT]</property>
    <property name="family">Bipolar Transistor</property>
    <property name="type">top view PNP (EBC)</property>
  </properties>
  <description>A standard PNP-transistor, top view for perf board use.</description>
  <spice>
    <model>*Typical bipolar transistor </model>
    <model>.MODEL PNP_GENERIC PNP ()</model>
    <line>Q{instanceTitle} {net connector2} {net connector1} {net connector0} PNP_GENERIC</line>
  </spice>
  <views>
    <iconView>
      <layers image="breadboard/top-view-transistor_signal_PNP_TO92_EBC_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/top-view-transistor_signal_PNP_TO92_EBC_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView fliphorizontal="true" flipvertical="true">
      <layers image="schematic/top-view-transistor_signal_PNP_TO92_EBC_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/TO92_3_100mil_1_pcb.svg">
        <layer layerId="copper0"/>
        <layer layerId="silkscreen"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" name="E" type="male">
      <description>Emitter</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector0pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector0pin" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector0pin"/>
          <p layer="copper1" svgId="connector0pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" name="B" type="male">
      <description>Base</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector1pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector1pin" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector1pin"/>
          <p layer="copper1" svgId="connector1pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" name="C" type="male">
      <description>Collector</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector2pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector2pin" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector2pin"/>
          <p layer="copper1" svgId="connector2pin"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
</module>
