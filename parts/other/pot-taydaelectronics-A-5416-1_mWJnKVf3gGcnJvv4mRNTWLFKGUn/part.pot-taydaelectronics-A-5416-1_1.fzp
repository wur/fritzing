<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module referenceFile="pot-taydaelectronics-A-5416-1_1.fzp" moduleId="pot-taydaelectronics-A-5416-1_1" fritzingVersion="0.9.9">
  <version>4</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>pot taydaelectronics A-5416-1</title>
  <label>R</label>
  <date>Thu Jan 20 2022</date>
  <tags>
    <tag>potentiometer</tag>
    <tag>poti</tag>
    <tag>adjustable</tag>
    <tag>resistor</tag>
    <tag>fritzing user</tag>
  </tags>
  <properties>
    <property name="family">potentiometer</property>
    <property name="type">taydaelectronics A-5416-1</property>
    <property showInLabel="yes" name="Maximum Resistance">10kΩ</property>
    <property name="Track">Linear</property>
    <property showInLabel="yes" name="power"/>
    <property name="package">THT</property>
    <property name="part number"/>
    <property name="layer"/>
    <property name="variant">variant 1</property>
  </properties>
  <taxonomy>discreteParts.resistors.adjustable.potentiometer</taxonomy>
  <description>A taydaelectronics A-5416-1 potentiometer.</description>
  <spice>
    <line>R{instanceTitle}A {net connector0} {net connector1} {{knob status}/100*{maximum resistance}}</line>
    <line>R{instanceTitle}B {net connector2} {net connector1} {{maximum resistance}-{knob status}/100*{maximum resistance}}</line>
  </spice>
  <views>
    <iconView>
      <layers image="breadboard/pot-taydaelectronics-A-5416-1_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/pot-taydaelectronics-A-5416-1_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView fliphorizontal="true" flipvertical="true">
      <layers image="schematic/pot-taydaelectronics-A-5416-1_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/pot-taydaelectronics-A-5416-1_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" type="male" name="leg1">
      <description>leg1</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" terminalId="connector0terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" type="male" name="wiper">
      <description>wiper</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" terminalId="connector1terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" type="male" name="leg2">
      <description>leg2</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" terminalId="connector2terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <url>https://www.taydaelectronics.com/datasheets/files/A-5415-1.pdf</url>
</module>
