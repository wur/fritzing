<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module moduleId="usb_power_control_1" referenceFile="usb_power_control_1.fzp" fritzingVersion="0.9.4">
  <version>1</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>USB Power Control</title>
  <label>Mod</label>
  <date>Thu Mar 4 2021</date>
  <url>https://www.switchdoc.com/usb-powercontrol-board/</url>
  <tags>
    <tag>contrib</tag>
    <tag>USB Power Control</tag>
    <tag>SwitchDoc Labs</tag>
    <tag>fritzing user</tag>
    <tag>Solar power</tag>
    <tag>lipo battery</tag>
  </tags>
  <properties>
    <property name="variant">variant 1</property>
    <property name="family">USB Power Control</property>
    <property name="editable pin labels">false</property>
    <property name="part number"/>
  </properties>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;SwitchDoc Labs USB PowerController. USB power input from a solar power contoller/LIPO battery combination to USB 5V output for a MCU such as the Raspberry PI. No pcb view as it isn't sensible.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/usb_power_control_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/usb_power_control_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/usb_power_control_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="breadboard/usb_power_control_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" name="GND-1" type="male">
      <description>USB in ground</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector0pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector0pin" terminalId="connector0terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector1" name="VIN" type="male">
      <description>USB in 5V</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector1pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector1pin" terminalId="connector1terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector2" name="pads-VOUT" type="male">
      <description>5V output pad</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector2pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector2pin" terminalId="connector2terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector3" type="male" name="GND-2">
      <description>ground output pad</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" layer="schematic" terminalId="connector3terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector4" type="male" name="control">
      <description>grove pin 1</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" layer="schematic" terminalId="connector4terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector5" type="male" name="enable">
      <description>grove pin 2</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" layer="schematic" terminalId="connector5terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector6" type="male" name="NC">
      <description>grove pin 3</description>
      <views>
        <breadboardView>
          <p svgId="connector6pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector6pin" layer="schematic" terminalId="connector6terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector7" type="male" name="GND-3">
      <description>grove pin 4</description>
      <views>
        <breadboardView>
          <p svgId="connector7pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector7pin" layer="schematic" terminalId="connector7terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector8" type="male" name="LIPOBATIN">
      <description>LIPO battery analog input</description>
      <views>
        <breadboardView>
          <p svgId="connector8pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector8pin" layer="schematic" terminalId="connector8terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector9" type="male" name="VOUT-USB">
      <description>5V output USB connector</description>
      <views>
        <breadboardView>
          <p svgId="connector9pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector9pin" layer="schematic" terminalId="connector9terminal"/>
        </schematicView>
      </views>
    </connector>
    <connector id="connector10" type="male" name="GND-4">
      <description>ground output USB connector</description>
      <views>
        <breadboardView>
          <p svgId="connector10pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector10pin" layer="schematic" terminalId="connector10terminal"/>
        </schematicView>
      </views>
    </connector>
  </connectors>
  <buses>
    <bus id="GND">
      <nodeMember connectorId="connector0"/>
      <nodeMember connectorId="connector3"/>
      <nodeMember connectorId="connector7"/>
      <nodeMember connectorId="connector10"/>
    </bus>
    <bus id="5VOUT">
      <nodeMember connectorId="connector2"/>
      <nodeMember connectorId="connector9"/>
    </bus>
  </buses>
</module>
