<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module referenceFile="Generic-male-header-8pins-2-row-1.27mm_601694f240f9d84de5c03f4856c53748_1.fzp" fritzingVersion="0.9.10" moduleId="Generic-male-header-8pins-2-row-1.27mm_601694f240f9d84de5c03f4856c53748_1">
  <version>1</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>Generic male header - 8 pins 2 rows 1.27mm pitch</title>
  <label>J</label>
  <date>Tue Jul 19 2022</date>
  <tags/>
  <properties>
    <property name="family">Pin Header 1.27mm</property>
    <property name="hole size"/>
    <property name="Pins">8</property>
    <property name="Form">♂ (male)</property>
    <property name="Pin Spacing">0.05in (1.27mm)</property>
    <property name="Row">double</property>
    <property name="package">THT</property>
    <property name="Position"/>
    <property name="layer"/>
    <property name="part number"/>
    <property name="variant">variant 1</property>
  </properties>
  <description>Generic male header 8 pin dual row 1.27mm pitch</description>
  <views>
    <iconView>
      <layers image="breadboard/Generic-male-header-8pins-2-row-1.27mm_601694f240f9d84de5c03f4856c53748_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/Generic-male-header-8pins-2-row-1.27mm_601694f240f9d84de5c03f4856c53748_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView fliphorizontal="true">
      <layers image="schematic/Generic-male-header-8pins-2-row-1.27mm_601694f240f9d84de5c03f4856c53748_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/Generic-male-header-8pins-2-row-1.27mm_601694f240f9d84de5c03f4856c53748_1_pcb.svg">
        <layer layerId="copper0"/>
        <layer layerId="silkscreen"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector name="pin1" type="male" id="connector0">
      <description>pin 1</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" terminalId="connector0terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin2" type="male" id="connector1">
      <description>pin 2</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" terminalId="connector1terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin3" type="male" id="connector2">
      <description>pin 3</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" terminalId="connector2terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin4" type="male" id="connector3">
      <description>pin 4</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" terminalId="connector3terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin5" type="male" id="connector4">
      <description>pin 5</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" terminalId="connector4terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin6" type="male" id="connector5">
      <description>pin 6</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" terminalId="connector5terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper0"/>
          <p svgId="connector5pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin7" type="male" id="connector6">
      <description>pin 7</description>
      <views>
        <breadboardView>
          <p svgId="connector6pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector6pin" terminalId="connector6terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector6pin" layer="copper0"/>
          <p svgId="connector6pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector name="pin8" type="male" id="connector7">
      <description>pin 8</description>
      <views>
        <breadboardView>
          <p svgId="connector7pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector7pin" terminalId="connector7terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector7pin" layer="copper0"/>
          <p svgId="connector7pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <url>https://www.mouser.com/datasheet/2/445/62200821121-1718193.pdf</url>
</module>
