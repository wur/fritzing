<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module moduleId="Atlas-sen206f-3-8in-flow-meter_1" referenceFile="Atlas-sen206f-3-8in-flow-meter_1.fzp" fritzingVersion="0.9.9">
  <version>1</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>Atlas sen206f 3/8in flow meter</title>
  <label>A</label>
  <date>Tue Apr 5 2022</date>
  <tags>
    <tag>contrib</tag>
    <tag>Atlas sen206f 3/8in flow meter</tag>
    <tag>Atlas</tag>
    <tag>fritzing user</tag>
    <tag>sensor</tag>
    <tag>flow meter</tag>
  </tags>
  <properties>
    <property name="variant">variant 1</property>
    <property name="family">Atlas sen206f 3/8in flow meter</property>
    <property name="editable pin labels">false</property>
    <property name="part number"/>
    <property name="pins">3</property>
    <property name="layer"/>
    <property name="package">THT</property>
    <property name="hole size"/>
    <property name="pin spacing">100mil</property>
    <property name="layout">Single Row</property>
  </properties>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;Our 3/8” male NPT turbine flow meters are compact, lightweight and allow for highly accurate and repeatable measurements during quick dispensing or metering cycles. This flow meter is intended for low flow ranges from 760 mL/min (0.2GPM) up to 7.6 L / min (4 GPM)..&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/Atlas-sen206f-3-8in-flow-meter_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/Atlas-sen206f-3-8in-flow-meter_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/Atlas-sen206f-3-8in-flow-meter_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/Atlas-sen206f-3-8in-flow-meter_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" name="pin1" type="male">
      <description>VCC (Red)</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector0pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector0pin" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector0pin"/>
          <p layer="copper1" svgId="connector0pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" name="pin2" type="male">
      <description>Pulse (White)</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector1pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector1pin" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector1pin"/>
          <p layer="copper1" svgId="connector1pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" name="pin3" type="male">
      <description>GND (Black)</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector2pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector2pin" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector2pin"/>
          <p layer="copper1" svgId="connector2pin"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <url>https://atlas-scientific.com/probes/3-8-flow-meter/#</url>
</module>
