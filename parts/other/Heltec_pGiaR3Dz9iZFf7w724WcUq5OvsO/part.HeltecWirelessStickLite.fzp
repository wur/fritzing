<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module fritzingVersion="0.9.4" referenceFile="Heltec Wireless Stick Lite.fzp" moduleId="HeltecWirelessStickLite">
  <version>1</version>
  <author>Peter Harrison</author>
  <title>Heltec Wireless Stick Lite</title>
  <label>Heltec Wireless Stick Lite</label>
  <date>Sun Jan 5 2020</date>
  <tags>
    <tag>Heltec</tag>
    <tag>Bluetooth</tag>
    <tag>Wirelss Stick</tag>
    <tag>Panda</tag>
    <tag>BLE</tag>
    <tag>WiFi</tag>
    <tag>LoRa</tag>
    <tag>LoRaWAN</tag>
  </tags>
  <properties>
    <property name="variant">variant 1</property>
    <property name="family">Heltec LoRa</property>
    <property name="part number">HTIT-WSL</property>
    <property name="module">Wireless Stick Lite</property>
    <property name="pins">40</property>
  </properties>
  <taxonomy>part.devmod.40.pins</taxonomy>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'.SF NS Text'; font-size:13pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;Wireless Stick Lite is a part of the Heltec LoRa series and incorporates an ESP32-Pico processor, WiFi, LoRa and Bluetooth with an integrated CP2102 USB to serial port interface and onboard SHT1.25 battery interface with integrated lithium battery management system.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="icon/HeltecWirelessStickLite0000_1_icon.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/HeltecWirelessStickLite0000_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/HeltecWirelessStickLite0000_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/HeltecWirelessStickLite0000_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector type="male" name="pin1" id="connector0">
      <description>GPIO11, Flash_SD2</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" terminalId="connector0terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin2" id="connector1">
      <description>GPIO10, U1_TXD</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" terminalId="connector1terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin3" id="connector2">
      <description>GPIO9, U1_RXD</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" terminalId="connector2terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin4" id="connector3">
      <description>GPIO25, ADC2_8, DAC2, LED</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" terminalId="connector3terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin5" id="connector4">
      <description>GPIO26, ADC2_9, DAC1, LoRa_DIO0</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" terminalId="connector4terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin6" id="connector5">
      <description>GPIO14, ADC2_6, Touch6, LoRa_RST</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" terminalId="connector5terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper0"/>
          <p svgId="connector5pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin7" id="connector6">
      <description>RST</description>
      <views>
        <breadboardView>
          <p svgId="connector6pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector6pin" terminalId="connector6terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector6pin" layer="copper0"/>
          <p svgId="connector6pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin8" id="connector7">
      <description>GPIO12, ADC2_5, Touch5</description>
      <views>
        <breadboardView>
          <p svgId="connector7pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector7pin" terminalId="connector7terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector7pin" layer="copper0"/>
          <p svgId="connector7pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 9" id="connector8">
      <description>GPIO13, ADC2_4, Touch4, Power Detection</description>
      <views>
        <breadboardView>
          <p svgId="connector8pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector8pin" terminalId="connector8terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector8pin" layer="copper0"/>
          <p svgId="connector8pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 10" id="connector9">
      <description>XTAL32, GPIO33, ADC1_5, Touch9</description>
      <views>
        <breadboardView>
          <p svgId="connector9pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector9pin" terminalId="connector9terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector9pin" layer="copper0"/>
          <p svgId="connector9pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 11" id="connector10">
      <description>XTAL32, GPIO32, ADC1_4, Touch8</description>
      <views>
        <breadboardView>
          <p svgId="connector10pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector10pin" terminalId="connector10terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector10pin" layer="copper0"/>
          <p svgId="connector10pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 12" id="connector11">
      <description>GPIO27, ADC2_7, Touch7, LoRa_MOSI</description>
      <views>
        <breadboardView>
          <p svgId="connector11pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector11pin" terminalId="connector11terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector11pin" layer="copper0"/>
          <p svgId="connector11pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 13" id="connector12">
      <description>GPIO15, ADC2_3, HSPI_CS0, Touch3</description>
      <views>
        <breadboardView>
          <p svgId="connector12pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector12pin" terminalId="connector12terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector12pin" layer="copper0"/>
          <p svgId="connector12pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 14" id="connector13">
      <description>GPIO2, ADC2_2, HSPI_WP, Touch2</description>
      <views>
        <breadboardView>
          <p svgId="connector13pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector13pin" terminalId="connector13terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector13pin" layer="copper0"/>
          <p svgId="connector13pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 15" id="connector14">
      <description>GPIO0, ADC2_1, Touch1, PRG_Button</description>
      <views>
        <breadboardView>
          <p svgId="connector14pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector14pin" terminalId="connector14terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector14pin" layer="copper0"/>
          <p svgId="connector14pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 16" id="connector15">
      <description>GPIO4, ADC2_0, HSPI_HD, Touch0</description>
      <views>
        <breadboardView>
          <p svgId="connector15pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector15pin" terminalId="connector15terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector15pin" layer="copper0"/>
          <p svgId="connector15pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 17" id="connector16">
      <description>GPIO16, U2_RXD, Flash_CS</description>
      <views>
        <breadboardView>
          <p svgId="connector16pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector16pin" terminalId="connector16terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector16pin" layer="copper0"/>
          <p svgId="connector16pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 18" id="connector17">
      <description>Ground 18</description>
      <views>
        <breadboardView>
          <p svgId="connector17pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector17pin" terminalId="connector17terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector17pin" layer="copper0"/>
          <p svgId="connector17pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 19" id="connector18">
      <description>3.3 Volts</description>
      <views>
        <breadboardView>
          <p svgId="connector18pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector18pin" terminalId="connector18terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector18pin" layer="copper0"/>
          <p svgId="connector18pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 20" id="connector19">
      <description>5 Volts</description>
      <views>
        <breadboardView>
          <p svgId="connector19pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector19pin" terminalId="connector19terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector19pin" layer="copper0"/>
          <p svgId="connector19pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 21" id="connector20">
      <description>Vext</description>
      <views>
        <breadboardView>
          <p svgId="connector20pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector20pin" terminalId="connector20terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector20pin" layer="copper0"/>
          <p svgId="connector20pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 22" id="connector21">
      <description>Ground 22</description>
      <views>
        <breadboardView>
          <p svgId="connector21pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector21pin" terminalId="connector21terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector21pin" layer="copper0"/>
          <p svgId="connector21pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 23" id="connector22">
      <description>SCL, GPIO22, V_SPI_WP, U0_RTS</description>
      <views>
        <breadboardView>
          <p svgId="connector22pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector22pin" terminalId="connector22terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector22pin" layer="copper0"/>
          <p svgId="connector22pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 24" id="connector23">
      <description>GPIO3, U0_RXD</description>
      <views>
        <breadboardView>
          <p svgId="connector23pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector23pin" terminalId="connector23terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector23pin" layer="copper0"/>
          <p svgId="connector23pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 25" id="connector24">
      <description>GPIO1, U0_TXD</description>
      <views>
        <breadboardView>
          <p svgId="connector24pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector24pin" terminalId="connector24terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector24pin" layer="copper0"/>
          <p svgId="connector24pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 26" id="connector25">
      <description>SDA, GPIO21, V_SPI_HD, Vext Control</description>
      <views>
        <breadboardView>
          <p svgId="connector25pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector25pin" terminalId="connector25terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector25pin" layer="copper0"/>
          <p svgId="connector25pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 27" id="connector26">
      <description>GPIO18, V_SPI_CLK, LoRa_CS</description>
      <views>
        <breadboardView>
          <p svgId="connector26pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector26pin" terminalId="connector26terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector26pin" layer="copper0"/>
          <p svgId="connector26pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 28" id="connector27">
      <description>GPIO23, V_SPI_D</description>
      <views>
        <breadboardView>
          <p svgId="connector27pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector27pin" terminalId="connector27terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector27pin" layer="copper0"/>
          <p svgId="connector27pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 29" id="connector28">
      <description>GPIO35, ADC1_7, LoRa_DIO1</description>
      <views>
        <breadboardView>
          <p svgId="connector28pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector28pin" terminalId="connector28terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector28pin" layer="copper0"/>
          <p svgId="connector28pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 30" id="connector29">
      <description>GPIO34, ADC1_6, LoRa_DIO2</description>
      <views>
        <breadboardView>
          <p svgId="connector29pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector29pin" terminalId="connector29terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector29pin" layer="copper0"/>
          <p svgId="connector29pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 31" id="connector30">
      <description>GPIO5, V_SPI, CS0, LoRa_SCK</description>
      <views>
        <breadboardView>
          <p svgId="connector30pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector30pin" terminalId="connector30terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector30pin" layer="copper0"/>
          <p svgId="connector30pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 32" id="connector31">
      <description>GPIO19, V_SPI_Q, U0_CTS, LoRa_MISO</description>
      <views>
        <breadboardView>
          <p svgId="connector31pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector31pin" terminalId="connector31terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector31pin" layer="copper0"/>
          <p svgId="connector31pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 33" id="connector32">
      <description>GPIO17, U2_TXD, Flash_SD0</description>
      <views>
        <breadboardView>
          <p svgId="connector32pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector32pin" terminalId="connector32terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector32pin" layer="copper0"/>
          <p svgId="connector32pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 34" id="connector33">
      <description>GPIO36, ADC1_0</description>
      <views>
        <breadboardView>
          <p svgId="connector33pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector33pin" terminalId="connector33terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector33pin" layer="copper0"/>
          <p svgId="connector33pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 35" id="connector34">
      <description>GPIO37, ADC1_1</description>
      <views>
        <breadboardView>
          <p svgId="connector34pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector34pin" terminalId="connector34terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector34pin" layer="copper0"/>
          <p svgId="connector34pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 36" id="connector35">
      <description>GPIO38, ADC1_2</description>
      <views>
        <breadboardView>
          <p svgId="connector35pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector35pin" terminalId="connector35terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector35pin" layer="copper0"/>
          <p svgId="connector35pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 37" id="connector36">
      <description>GPIO39, ADC1_3</description>
      <views>
        <breadboardView>
          <p svgId="connector36pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector36pin" terminalId="connector36terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector36pin" layer="copper0"/>
          <p svgId="connector36pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 38" id="connector37">
      <description>GPIO8, Flash_SD1</description>
      <views>
        <breadboardView>
          <p svgId="connector37pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector37pin" terminalId="connector37terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector37pin" layer="copper0"/>
          <p svgId="connector37pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 39" id="connector38">
      <description>GPIO7, Flash_SD3</description>
      <views>
        <breadboardView>
          <p svgId="connector38pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector38pin" terminalId="connector38terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector38pin" layer="copper0"/>
          <p svgId="connector38pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" name="pin 40" id="connector39">
      <description>GPIO6, Flash_CLK</description>
      <views>
        <breadboardView>
          <p svgId="connector39pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector39pin" terminalId="connector39terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector39pin" layer="copper0"/>
          <p svgId="connector39pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <url>http://heltec.org</url>
  <buses>
    <bus id="GND">
      <nodeMember connectorId="connector21"/>
      <nodeMember connectorId="connector17"/>
    </bus>
  </buses>
</module>
