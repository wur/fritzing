<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module fritzingVersion="0.9.4" moduleId="BF245_1">
  <version>1</version>
  <author>Carlos Martinez Ubeda (modified by vanepp May 2020)</author>
  <title>BF245 JFET N-Channel</title>
  <label>Q</label>
  <date>2020-05-09</date>
  <tags>
    <tag>Transistor</tag>
    <tag>FET</tag>
    <tag>BF245</tag>
    <tag>JFET</tag>
    <tag>TO92</tag>
    <tag>TO-92</tag>
    <tag>TRANSISTOR_JFET</tag>
  </tags>
  <properties>
    <property name="package">TO92</property>
    <property name="family">JFET-Transistor</property>
    <property name="type">N-Channel (GSD)</property>
    <property name="part number">BF245</property>
  </properties>
  <description>A BF245 JFET N-Channel Transistor. GSD configuration.</description>
  <views>
    <iconView>
      <layers image="breadboard/BF245_1.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/BF245_1.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/BF245_1.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/BF245_1.svg">
        <layer layerId="copper1"/>
        <layer layerId="copper0"/>
        <layer layerId="silkscreen"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" type="male" name="gate">
      <description>gate</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector0pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector0pin" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector0pin"/>
          <p layer="copper1" svgId="connector0pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" type="male" name="source">
      <description>source</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector1pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector1pin" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector1pin"/>
          <p layer="copper1" svgId="connector1pin"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" type="male" name="drain">
      <description>drain</description>
      <views>
        <breadboardView>
          <p layer="breadboard" svgId="connector2pin"/>
        </breadboardView>
        <schematicView>
          <p layer="schematic" svgId="connector2pin" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p layer="copper0" svgId="connector2pin"/>
          <p layer="copper1" svgId="connector2pin"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
</module>
