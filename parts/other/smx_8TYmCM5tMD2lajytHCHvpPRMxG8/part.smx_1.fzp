<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module moduleId="smx_1" referenceFile="smx_1.fzp" fritzingVersion="0.9.6">
  <version>1</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>smx</title>
  <label>M</label>
  <date>Fri Jul 23 2021</date>
  <tags>
    <tag>contrib</tag>
    <tag>smx</tag>
    <tag>moisture sensor</tag>
    <tag>fritzing user</tag>
    <tag>sensor</tag>
  </tags>
  <properties>
    <property name="variant">variant 1</property>
    <property name="family">smx</property>
    <property name="editable pin labels">false</property>
    <property name="part number"/>
    <property name="layer"/>
  </properties>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;The SMX is an interface module for sensors that measure the electrical resistance of moist substrates. It provides three types of outputs, allowing the sensor to easily interface with a wide
range of general purpose data logging equipment and meters. Additionally, it prevents corrosion of the sensor (using AC excitation), and galvanically isolates it from unwanted underground currents.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/smx_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/smx_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/smx_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/smx_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" type="male" name="pin 1">
      <description>GND ground</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" layer="schematic" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" type="male" name="pin 2">
      <description>VCC 3.3V to 15V power input</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" layer="schematic" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" type="male" name="pin 3">
      <description>Sig-Out digital signal output (open collector)</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" layer="schematic" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" type="male" name="pin 4">
      <description>Volt-Out voltage output from the sensor</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" layer="schematic" terminalId="connector3terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector4" type="male" name="pin 5">
      <description>Sensor1</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" layer="schematic" terminalId="connector4terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector5" type="male" name="pin 6">
      <description>Sensor2</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" layer="schematic" terminalId="connector5terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper0"/>
          <p svgId="connector5pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <url>https://www.emesystems.com/smx/main.html</url>
</module>
