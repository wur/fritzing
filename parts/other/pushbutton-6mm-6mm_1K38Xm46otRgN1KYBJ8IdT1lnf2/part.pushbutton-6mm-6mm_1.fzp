<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module fritzingVersion="0.9.9" moduleId="pushbutton-6mm-6mm_1" referenceFile="pushbutton-6mm-6mm_1.fzp">
  <version>1</version>
  <title>Pushbutton 6mm-6mm</title>
  <author>Peter Van Epp (vanepp in forums)</author>
  <label>S</label>
  <date>Tue May 24 2022</date>
  <tags>
    <tag>Pushbutton</tag>
    <tag>button</tag>
    <tag>fritzing user</tag>
    <tag>contrib</tag>
  </tags>
  <properties>
    <property name="family">switch</property>
    <property name="package">[THT]</property>
    <property name="switch status">Released</property>
    <property name="part number"/>
    <property name="layer"/>
    <property name="variant">variant 6mm-6mm</property>
  </properties>
  <description>A generic pushbutton.</description>
  <spice>
    <line>S{instanceTitle} {net connector0} {net connector2} control_{instanceTitle} 0 generic_switch OFF</line>
    <line>V{instanceTitle} control_{instanceTitle} 0 DC 0</line>
    <model>.MODEL generic_switch SW(Ron=1m Roff=1Meg Vt=2.5)</model>
  </spice>
  <views>
    <iconView>
      <layers image="breadboard/pushbutton-6mm-6mm_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/pushbutton-6mm-6mm_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView flipvertical="true" fliphorizontal="true">
      <layers image="schematic/pushbutton-6mm-6mm_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/pushbutton-6mm-6mm_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector name="Pin 1" id="connector0" type="male">
      <description>pin 1 is always connected with pin 2</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" terminalId="connector0terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector name="Pin 2" id="connector1" type="male">
      <description>pin 2 is always connected with pin 1</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" terminalId="connector1terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector name="Pin 3" id="connector2" type="male">
      <description>pin 3 is always connected with pin 4</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" terminalId="connector2terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector name="Pin 4" id="connector3" type="male">
      <description>pin 4 is always connected with pin 3</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" terminalId="connector3terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <buses>
    <bus id="bus0">
      <nodeMember connectorId="connector0"/>
      <nodeMember connectorId="connector1"/>
    </bus>
    <bus id="bus1">
      <nodeMember connectorId="connector2"/>
      <nodeMember connectorId="connector3"/>
    </bus>
  </buses>
  <url>https://ch.schurter.com/de/datasheet/typ_6x6_mm_Kurzhubtaster.pdf</url>
</module>
