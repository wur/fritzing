<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module moduleId="LOLIN-D1-mini-v3.1.0_1" fritzingVersion="0.9.9" referenceFile="LOLIN-D1-mini-v3.1.0_1.fzp">
  <version>1</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>WeMos D1 Mini V3.1.0</title>
  <label>M</label>
  <date>Sat Dec 25 2021</date>
  <url>https://www.wemos.cc/en/latest/d1/d1_mini_3.1.0.html</url>
  <tags>
    <tag>Microcontroller</tag>
    <tag>ESP-8266</tag>
    <tag>WiFi</tag>
  </tags>
  <properties>
    <property name="family">WeMos D1 Mini V3.1.0</property>
    <property name="variant">variant 1</property>
    <property name="cpu">ESP-8266EX</property>
    <property name="layer"/>
    <property name="part number"/>
  </properties>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;The D1 mini is a mini wifi board based on ESP-8266EX.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/LOLIN-D1-mini-v3.1.0_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/LOLIN-D1-mini-v3.1.0_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/LOLIN-D1-mini-v3.1.0_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/LOLIN-D1-mini-v3.1.0_1_pcb.svg">
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
        <layer layerId="silkscreen"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector type="male" id="connector0" name="RST">
      <description>RST</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" terminalId="connector0terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector1" name="A0">
      <description>A0</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" terminalId="connector1terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector2" name="D0">
      <description>D0</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" terminalId="connector2terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector3" name="D5">
      <description>D5</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" terminalId="connector3terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector4" name="D6">
      <description>D6</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" terminalId="connector4terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector5" name="D7">
      <description>D7</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" terminalId="connector5terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper0"/>
          <p svgId="connector5pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector6" name="D8">
      <description>D8</description>
      <views>
        <breadboardView>
          <p svgId="connector6pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector6pin" terminalId="connector6terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector6pin" layer="copper0"/>
          <p svgId="connector6pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector7" name="3V3">
      <description>3V3</description>
      <views>
        <breadboardView>
          <p svgId="connector7pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector7pin" terminalId="connector7terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector7pin" layer="copper0"/>
          <p svgId="connector7pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector8" name="5V">
      <description>5V</description>
      <views>
        <breadboardView>
          <p svgId="connector8pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector8pin" terminalId="connector8terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector8pin" layer="copper0"/>
          <p svgId="connector8pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector9" name="G">
      <description>G</description>
      <views>
        <breadboardView>
          <p svgId="connector9pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector9pin" terminalId="connector9terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector9pin" layer="copper0"/>
          <p svgId="connector9pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector10" name="D4">
      <description>D4</description>
      <views>
        <breadboardView>
          <p svgId="connector10pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector10pin" terminalId="connector10terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector10pin" layer="copper0"/>
          <p svgId="connector10pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector11" name="D3">
      <description>D3</description>
      <views>
        <breadboardView>
          <p svgId="connector11pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector11pin" terminalId="connector11terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector11pin" layer="copper0"/>
          <p svgId="connector11pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector12" name="D2">
      <description>D2</description>
      <views>
        <breadboardView>
          <p svgId="connector12pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector12pin" terminalId="connector12terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector12pin" layer="copper0"/>
          <p svgId="connector12pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector13" name="D1">
      <description>D1</description>
      <views>
        <breadboardView>
          <p svgId="connector13pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector13pin" terminalId="connector13terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector13pin" layer="copper0"/>
          <p svgId="connector13pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector14" name="RX">
      <description>RX</description>
      <views>
        <breadboardView>
          <p svgId="connector14pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector14pin" terminalId="connector14terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector14pin" layer="copper0"/>
          <p svgId="connector14pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector type="male" id="connector15" name="TX">
      <description>TX</description>
      <views>
        <breadboardView>
          <p svgId="connector15pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector15pin" terminalId="connector15terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector15pin" layer="copper0"/>
          <p svgId="connector15pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
</module>
