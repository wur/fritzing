<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module referenceFile="TDA7439_18a58d0be33b96f49e4f6162c2da194d_1.fzp" moduleId="TDA7439_18a58d0be33b96f49e4f6162c2da194d_1" fritzingVersion="0.9.4">
  <version>4</version>
  <author>Peter Van Epp (vanepp in forums)</author>
  <title>TDA7439</title>
  <label>U</label>
  <date>Sat Mar 7 2020</date>
  <tags>
    <tag>audio processor</tag>
    <tag>DIP</tag>
    <tag>fritzing user</tag>
    <tag>TDA7439</tag>
  </tags>
  <properties>
    <property name="variant">variant 1</property>
    <property name="family">TDA7439</property>
    <property name="hole size"/>
    <property name="pins">30</property>
    <property name="package">DIP (Dual Inline) [THT]</property>
    <property name="pin spacing">400mil</property>
    <property name="layer"/>
    <property name="editable pin labels">false</property>
    <property name="chip label">TDA7439</property>
    <property name="part number"/>
  </properties>
  <taxonomy>part.dip.30.pins</taxonomy>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;&lt;span style=" font-family:'sans-serif';"&gt;The TDA7439 is a volume, tone (bass, mid-range and treble) and balance (left/right) processor for high-quality audio applications in car-radio and Hi-Fi systems. Selectable input gain is provided. All the functions are controlled by serial bus.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/TDA7439_18a58d0be33b96f49e4f6162c2da194d_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/TDA7439_18a58d0be33b96f49e4f6162c2da194d_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/TDA7439_18a58d0be33b96f49e4f6162c2da194d_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/TDA7439_18a58d0be33b96f49e4f6162c2da194d_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" type="male" name="pin1">
      <description>pin 1</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard" terminalId="connector0terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" layer="schematic" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" type="male" name="pin2">
      <description>pin 2</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard" terminalId="connector1terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" layer="schematic" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" type="male" name="pin3">
      <description>pin 3</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard" terminalId="connector2terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" layer="schematic" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" type="male" name="pin4">
      <description>pin 4</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard" terminalId="connector3terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" layer="schematic" terminalId="connector3terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector4" type="male" name="pin5">
      <description>pin 5</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard" terminalId="connector4terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" layer="schematic" terminalId="connector4terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector5" type="male" name="pin6">
      <description>pin 6</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard" terminalId="connector5terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" layer="schematic" terminalId="connector5terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper0"/>
          <p svgId="connector5pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector6" type="male" name="pin7">
      <description>pin 7</description>
      <views>
        <breadboardView>
          <p svgId="connector6pin" layer="breadboard" terminalId="connector6terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector6pin" layer="schematic" terminalId="connector6terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector6pin" layer="copper0"/>
          <p svgId="connector6pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector7" type="male" name="pin8">
      <description>pin 8</description>
      <views>
        <breadboardView>
          <p svgId="connector7pin" layer="breadboard" terminalId="connector7terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector7pin" layer="schematic" terminalId="connector7terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector7pin" layer="copper0"/>
          <p svgId="connector7pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector8" type="male" name="pin9">
      <description>pin 9</description>
      <views>
        <breadboardView>
          <p svgId="connector8pin" layer="breadboard" terminalId="connector8terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector8pin" layer="schematic" terminalId="connector8terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector8pin" layer="copper0"/>
          <p svgId="connector8pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector9" type="male" name="pin10">
      <description>pin 10</description>
      <views>
        <breadboardView>
          <p svgId="connector9pin" layer="breadboard" terminalId="connector9terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector9pin" layer="schematic" terminalId="connector9terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector9pin" layer="copper0"/>
          <p svgId="connector9pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector10" type="male" name="pin11">
      <description>pin 11</description>
      <views>
        <breadboardView>
          <p svgId="connector10pin" layer="breadboard" terminalId="connector10terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector10pin" layer="schematic" terminalId="connector10terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector10pin" layer="copper0"/>
          <p svgId="connector10pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector11" type="male" name="pin12">
      <description>pin 12</description>
      <views>
        <breadboardView>
          <p svgId="connector11pin" layer="breadboard" terminalId="connector11terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector11pin" layer="schematic" terminalId="connector11terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector11pin" layer="copper0"/>
          <p svgId="connector11pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector12" type="male" name="pin13">
      <description>pin 13</description>
      <views>
        <breadboardView>
          <p svgId="connector12pin" layer="breadboard" terminalId="connector12terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector12pin" layer="schematic" terminalId="connector12terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector12pin" layer="copper0"/>
          <p svgId="connector12pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector13" type="male" name="pin14">
      <description>pin 14</description>
      <views>
        <breadboardView>
          <p svgId="connector13pin" layer="breadboard" terminalId="connector13terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector13pin" layer="schematic" terminalId="connector13terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector13pin" layer="copper0"/>
          <p svgId="connector13pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector14" type="male" name="pin15">
      <description>pin 15</description>
      <views>
        <breadboardView>
          <p svgId="connector14pin" layer="breadboard" terminalId="connector14terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector14pin" layer="schematic" terminalId="connector14terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector14pin" layer="copper0"/>
          <p svgId="connector14pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector15" type="male" name="pin16">
      <description>pin 16</description>
      <views>
        <breadboardView>
          <p svgId="connector15pin" layer="breadboard" terminalId="connector15terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector15pin" layer="schematic" terminalId="connector15terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector15pin" layer="copper0"/>
          <p svgId="connector15pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector16" type="male" name="pin17">
      <description>pin 17</description>
      <views>
        <breadboardView>
          <p svgId="connector16pin" layer="breadboard" terminalId="connector16terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector16pin" layer="schematic" terminalId="connector16terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector16pin" layer="copper0"/>
          <p svgId="connector16pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector17" type="male" name="pin18">
      <description>pin 18</description>
      <views>
        <breadboardView>
          <p svgId="connector17pin" layer="breadboard" terminalId="connector17terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector17pin" layer="schematic" terminalId="connector17terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector17pin" layer="copper0"/>
          <p svgId="connector17pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector18" type="male" name="pin19">
      <description>pin 19</description>
      <views>
        <breadboardView>
          <p svgId="connector18pin" layer="breadboard" terminalId="connector18terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector18pin" layer="schematic" terminalId="connector18terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector18pin" layer="copper0"/>
          <p svgId="connector18pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector19" type="male" name="pin20">
      <description>pin 20</description>
      <views>
        <breadboardView>
          <p svgId="connector19pin" layer="breadboard" terminalId="connector19terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector19pin" layer="schematic" terminalId="connector19terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector19pin" layer="copper0"/>
          <p svgId="connector19pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector20" type="male" name="pin21">
      <description>pin 21</description>
      <views>
        <breadboardView>
          <p svgId="connector20pin" layer="breadboard" terminalId="connector20terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector20pin" layer="schematic" terminalId="connector20terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector20pin" layer="copper0"/>
          <p svgId="connector20pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector21" type="male" name="pin22">
      <description>pin 22</description>
      <views>
        <breadboardView>
          <p svgId="connector21pin" layer="breadboard" terminalId="connector21terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector21pin" layer="schematic" terminalId="connector21terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector21pin" layer="copper0"/>
          <p svgId="connector21pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector22" type="male" name="pin23">
      <description>pin 23</description>
      <views>
        <breadboardView>
          <p svgId="connector22pin" layer="breadboard" terminalId="connector22terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector22pin" layer="schematic" terminalId="connector22terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector22pin" layer="copper0"/>
          <p svgId="connector22pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector23" type="male" name="pin24">
      <description>pin 24</description>
      <views>
        <breadboardView>
          <p svgId="connector23pin" layer="breadboard" terminalId="connector23terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector23pin" layer="schematic" terminalId="connector23terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector23pin" layer="copper0"/>
          <p svgId="connector23pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector24" type="male" name="pin25">
      <description>pin 25</description>
      <views>
        <breadboardView>
          <p svgId="connector24pin" layer="breadboard" terminalId="connector24terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector24pin" layer="schematic" terminalId="connector24terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector24pin" layer="copper0"/>
          <p svgId="connector24pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector25" type="male" name="pin26">
      <description>pin 26</description>
      <views>
        <breadboardView>
          <p svgId="connector25pin" layer="breadboard" terminalId="connector25terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector25pin" layer="schematic" terminalId="connector25terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector25pin" layer="copper0"/>
          <p svgId="connector25pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector26" type="male" name="pin27">
      <description>pin 27</description>
      <views>
        <breadboardView>
          <p svgId="connector26pin" layer="breadboard" terminalId="connector26terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector26pin" layer="schematic" terminalId="connector26terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector26pin" layer="copper0"/>
          <p svgId="connector26pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector27" type="male" name="pin28">
      <description>pin 28</description>
      <views>
        <breadboardView>
          <p svgId="connector27pin" layer="breadboard" terminalId="connector27terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector27pin" layer="schematic" terminalId="connector27terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector27pin" layer="copper0"/>
          <p svgId="connector27pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector28" type="male" name="pin29">
      <description>pin 29</description>
      <views>
        <breadboardView>
          <p svgId="connector28pin" layer="breadboard" terminalId="connector28terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector28pin" layer="schematic" terminalId="connector28terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector28pin" layer="copper0"/>
          <p svgId="connector28pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector29" type="male" name="pin30">
      <description>pin 30</description>
      <views>
        <breadboardView>
          <p svgId="connector29pin" layer="breadboard" terminalId="connector29terminal"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector29pin" layer="schematic" terminalId="connector29terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector29pin" layer="copper0"/>
          <p svgId="connector29pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <url>file:///C:/Users/owner/AppData/Local/Temp/cd00004906-1.pdf</url>
</module>
