<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module fritzingVersion="0.9.6" moduleId="Atlas-RTD-temperature-Circuit_1" referenceFile="Atlas-RTD-temperature-Circuit_1.fzp">
  <author>Atlas Scientific (modified by vanepp Apr 2021)</author>
  <title>Atlas Conductivity Circuit</title>
  <label>Mod</label>
  <date>Fri Apr 23 2021</date>
  <tags/>
  <properties>
    <property name="family">Atlas-RTD-temperature-Circuit</property>
    <property name="variant">variant 1</property>
    <property name="part number"/>
    <property name="layer"/>
  </properties>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;&#13;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;&#13;
p, li { white-space: pre-wrap; }&#13;
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;&#13;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;&lt;span style=" font-size:8pt;"&gt;There is no shortage of temperature probes out there, and the most accurate of all is the platinum RTD probe. But, converting the resistance of platinum to an actual temperature is unusually complicated. Fortunately, the Atlas Scientific EZO-RTD Circuit makes taking high accuracy readings from a platinum RTD probes easy. The EZO-RTD Circuit can work with any class of 2, 3, or 4 wire platinum RTD probes and has a sensing range of -126.000 °C to +1,254 °C&lt;/span&gt;&lt;/p&gt;&#13;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/Atlas-RTD-temperature-Circuit_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <schematicView>
      <layers image="schematic/Atlas-RTD-temperature-Circuit_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <breadboardView>
      <layers image="breadboard/Atlas-RTD-temperature-Circuit_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <pcbView>
      <layers image="pcb/Atlas-RTD-temperature-Circuit_1_pcb.svg">
        <layer layerId="copper0"/>
        <layer layerId="silkscreen"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" type="male" name="GND">
      <description>Supply Ground</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" layer="schematic" terminalId="connector0terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper1"/>
          <p svgId="connector0pin" layer="copper0"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" type="male" name="TX">
      <description>Data Out</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" layer="schematic" terminalId="connector1terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper1"/>
          <p svgId="connector1pin" layer="copper0"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" type="male" name="RX">
      <description>Data In</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" layer="schematic" terminalId="connector2terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper1"/>
          <p svgId="connector2pin" layer="copper0"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" type="male" name="Vcc">
      <description>Supply Voltage</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" layer="schematic" terminalId="connector3terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper1"/>
          <p svgId="connector3pin" layer="copper0"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector4" type="male" name="PRB">
      <description>Probe (Sensor)</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" layer="schematic" terminalId="connector4terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper1"/>
          <p svgId="connector4pin" layer="copper0"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector5" type="male" name="PGND">
      <description>Probe (Sensor)</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" layer="schematic" terminalId="connector5terminal"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper1"/>
          <p svgId="connector5pin" layer="copper0"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <url>https://atlas-scientific.com/embedded-solutions/ezo-rtd-temperature-circuit/#</url>
</module>
