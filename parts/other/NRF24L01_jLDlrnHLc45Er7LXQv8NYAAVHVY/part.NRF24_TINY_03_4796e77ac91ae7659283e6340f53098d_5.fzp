<?xml version='1.0' encoding='UTF-8'?>
<module moduleId="NRF24_TINY_03_4796e77ac91ae7659283e6340f53098d_5" fritzingVersion="0.5.2b.02.18.4756" referenceFile="NRF24L01+_breakout.fzp">
 <version>5</version>
 <date>週四 一月 28 2021</date>
 <author>Po Ting Chen, Panda'Spectrum</author>
 <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
&lt;html>&lt;head>&lt;meta name="qrichtext" content="1" />&lt;style type="text/css">
p, li { white-space: pre-wrap; }
&lt;/style>&lt;/head>&lt;body style=" font-family:'PMingLiU'; font-size:9pt; font-weight:400; font-style:normal;">
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">&lt;span style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt;">This module stamp is for the Nordic Semiconductor NRF24L01+ 2.4GHz RF transceiver IC.&lt;br />Origined from Richard Bruneau, Fabian Althaus.&lt;/span>&lt;/p>
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">&lt;span style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt;">&lt;br />2021: PCB part Modified by Po Ting Chen, Panda'Spectrum of Taiwan, for tiny footprint module.&lt;/span>&lt;/p>
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">&lt;span style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt;">ask fb.me/pandaspectrum for details &lt;/span>&lt;/p>
&lt;p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'MS Shell Dlg 2'; font-size:8.25pt;">&lt;br />&lt;/p>
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">&lt;span style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt;">ver_2 - with breadboard edit&lt;/span>&lt;/p>&lt;/body>&lt;/html></description>
 <title>NRF24L01+ Tiny Module</title>
 <url/>
 <tags>
  <tag>transceiver</tag>
  <tag>NRF24L01+</tag>
  <tag>rf</tag>
  <tag>breakout</tag>
 </tags>
 <properties>
  <property name="variant">variant 3</property>
  <property name="family">2.4ghz transceiver</property>
  <property name="chip">NRF24L01+</property>
  <property name="part number"></property>
  <property name="layer"></property>
 </properties>
 <views>
  <breadboardView>
   <layers image="breadboard/NRF24_TINY_03_4796e77ac91ae7659283e6340f53098d_5_breadboard.svg">
    <layer layerId="breadboard"/>
   </layers>
  </breadboardView>
  <schematicView>
   <layers image="schematic/NRF24_TINY_03_4796e77ac91ae7659283e6340f53098d_5_schematic.svg">
    <layer layerId="schematic"/>
   </layers>
  </schematicView>
  <pcbView>
   <layers image="pcb/NRF24_TINY_03_4796e77ac91ae7659283e6340f53098d_5_pcb.svg">
    <layer layerId="copper1"/>
    <layer layerId="silkscreen"/>
    <layer layerId="copper0"/>
   </layers>
  </pcbView>
  <iconView>
   <layers image="icon/NRF24_TINY_03_4796e77ac91ae7659283e6340f53098d_5_icon.svg">
    <layer layerId="icon"/>
   </layers>
  </iconView>
 </views>
 <connectors>
  <connector name="GND" type="female" id="connector8">
   <description>GND</description>
   <views>
    <breadboardView>
     <p svgId="connector8pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector8pin" terminalId="connector8terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector8pad" layer="copper0"/>
     <p svgId="connector8pad" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="VCC" type="female" id="connector9">
   <description>VCC</description>
   <views>
    <breadboardView>
     <p svgId="connector9pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector9pin" terminalId="connector9terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector9pad" layer="copper0"/>
     <p svgId="connector9pad" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="CE" type="female" id="connector10">
   <description>CE</description>
   <views>
    <breadboardView>
     <p svgId="connector10pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector10pin" terminalId="connector10terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector10pad" layer="copper0"/>
     <p svgId="connector10pad" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="CS" type="female" id="connector11">
   <description>CS</description>
   <views>
    <breadboardView>
     <p svgId="connector11pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector11pin" terminalId="connector11terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector11pad" layer="copper0"/>
     <p svgId="connector11pad" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="SCK" type="female" id="connector12">
   <description>SCK</description>
   <views>
    <breadboardView>
     <p svgId="connector12pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector12pin" terminalId="connector12terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector12pad" layer="copper0"/>
     <p svgId="connector12pad" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="MOSI" type="female" id="connector13">
   <description>MOSI</description>
   <views>
    <breadboardView>
     <p svgId="connector13pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector13pin" terminalId="connector13terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector13pad" layer="copper0"/>
     <p svgId="connector13pad" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="MISO" type="female" id="connector14">
   <description>MISO</description>
   <views>
    <breadboardView>
     <p svgId="connector14pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector14pin" terminalId="connector14terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector14pin" layer="copper0"/>
     <p svgId="connector14pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
  <connector name="IRQ" type="female" id="connector15">
   <description>IRQ</description>
   <views>
    <breadboardView>
     <p svgId="connector15pin" layer="breadboard"/>
    </breadboardView>
    <schematicView>
     <p svgId="connector15pin" terminalId="connector15terminal" layer="schematic"/>
    </schematicView>
    <pcbView>
     <p svgId="connector15pin" layer="copper0"/>
     <p svgId="connector15pin" layer="copper1"/>
    </pcbView>
   </views>
  </connector>
 </connectors>
 <buses/>
</module>
