<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<module moduleId="74x126_1" referenceFile="74x126_1.fzp" fritzingVersion="0.9.6">
  <version>1</version>
  <author>Peter Van Epp (vanepp in the forums)</author>
  <title>74x126</title>
  <label>U</label>
  <date>Sun May 2 2021</date>
  <tags>
    <tag>DIP</tag>
    <tag>non inverting</tag>
    <tag>tristate buffer</tag>
    <tag>tristate</tag>
    <tag>fritzing user</tag>
    <tag>DIP14</tag>
    <tag>logic gates</tag>
    <tag>subpart</tag>
  </tags>
  <properties>
    <property name="variant">1</property>
    <property name="family">74x126</property>
    <property name="chip label">74x126</property>
    <property name="part number">74x126</property>
    <property name="inputs per device">2</property>
    <property name="editable pin labels">false</property>
    <property name="devices">4</property>
    <property name="pin spacing">300mil</property>
    <property name="pins">14</property>
    <property name="package">DIP (Dual Inline) [THT]</property>
    <property name="hole size"/>
    <property name="layer"/>
  </properties>
  <taxonomy>part.dip.14.pins</taxonomy>
  <description>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd"&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name="qrichtext" content="1" /&gt;&lt;style type="text/css"&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;"&gt;
&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"&gt;&lt;span style=" font-family:'Oxygen-Sans'; font-size:10pt;"&gt;74x126 quad tristate buffer active high enable. The 74ahc126 variety is useful as a level translator as the inputs are 5V tolorant when run with a VCC of 3.3V (and will thus take a 5V signal in and provide a 3.3V signal out.) As well this part uses schematic subparts so you can drag each of the 4 gates to a different place in schematic view. Note having many (around 10 or more) subpart parts in a sketch causes Fritzing to slow down substantially.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</description>
  <views>
    <iconView>
      <layers image="breadboard/74x126_1_breadboard.svg">
        <layer layerId="icon"/>
      </layers>
    </iconView>
    <breadboardView>
      <layers image="breadboard/74x126_1_breadboard.svg">
        <layer layerId="breadboard"/>
      </layers>
    </breadboardView>
    <schematicView>
      <layers image="schematic/74x126_1_schematic.svg">
        <layer layerId="schematic"/>
      </layers>
    </schematicView>
    <pcbView>
      <layers image="pcb/74x126_1_pcb.svg">
        <layer layerId="silkscreen"/>
        <layer layerId="copper0"/>
        <layer layerId="copper1"/>
      </layers>
    </pcbView>
  </views>
  <connectors>
    <connector id="connector0" type="male" name="C1">
      <description>Control 1</description>
      <views>
        <breadboardView>
          <p svgId="connector0pin" terminalId="connector0terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector0pin" terminalId="connector0terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector0pin" layer="copper0"/>
          <p svgId="connector0pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector1" type="male" name="A1">
      <description>Input 1</description>
      <views>
        <breadboardView>
          <p svgId="connector1pin" terminalId="connector1terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector1pin" terminalId="connector1terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector1pin" layer="copper0"/>
          <p svgId="connector1pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector2" type="male" name="Y1">
      <description>Output 1</description>
      <views>
        <breadboardView>
          <p svgId="connector2pin" terminalId="connector2terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector2pin" terminalId="connector2terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector2pin" layer="copper0"/>
          <p svgId="connector2pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector3" type="male" name="C2">
      <description>Control 2</description>
      <views>
        <breadboardView>
          <p svgId="connector3pin" terminalId="connector3terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector3pin" terminalId="connector3terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector3pin" layer="copper0"/>
          <p svgId="connector3pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector4" type="male" name="A2">
      <description>Input 2</description>
      <views>
        <breadboardView>
          <p svgId="connector4pin" terminalId="connector4terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector4pin" terminalId="connector4terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector4pin" layer="copper0"/>
          <p svgId="connector4pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector5" type="male" name="Y2">
      <description>Output 2</description>
      <views>
        <breadboardView>
          <p svgId="connector5pin" terminalId="connector5terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector5pin" terminalId="connector5terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector5pin" layer="copper0"/>
          <p svgId="connector5pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector6" type="male" name="GND">
      <description>GND</description>
      <views>
        <breadboardView>
          <p svgId="connector6pin" terminalId="connector6terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector6pin" terminalId="connector6terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector6pin" layer="copper0"/>
          <p svgId="connector6pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector7" type="male" name="Y3">
      <description>Output 3</description>
      <views>
        <breadboardView>
          <p svgId="connector7pin" terminalId="connector7terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector7pin" terminalId="connector7terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector7pin" layer="copper0"/>
          <p svgId="connector7pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector8" type="male" name="A3">
      <description>Input 3</description>
      <views>
        <breadboardView>
          <p svgId="connector8pin" terminalId="connector8terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector8pin" terminalId="connector8terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector8pin" layer="copper0"/>
          <p svgId="connector8pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector9" type="male" name="C3">
      <description>Control 3</description>
      <views>
        <breadboardView>
          <p svgId="connector9pin" terminalId="connector9terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector9pin" terminalId="connector9terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector9pin" layer="copper0"/>
          <p svgId="connector9pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector10" type="male" name="Y4">
      <description>Output 4</description>
      <views>
        <breadboardView>
          <p svgId="connector10pin" terminalId="connector10terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector10pin" terminalId="connector10terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector10pin" layer="copper0"/>
          <p svgId="connector10pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector11" type="male" name="A4">
      <description>A4</description>
      <views>
        <breadboardView>
          <p svgId="connector11pin" terminalId="connector11terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector11pin" terminalId="connector11terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector11pin" layer="copper0"/>
          <p svgId="connector11pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector12" type="male" name="C4">
      <description>Control 4</description>
      <views>
        <breadboardView>
          <p svgId="connector12pin" terminalId="connector12terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector12pin" terminalId="connector12terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector12pin" layer="copper0"/>
          <p svgId="connector12pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
    <connector id="connector13" type="male" name="VCC">
      <description>VCC</description>
      <views>
        <breadboardView>
          <p svgId="connector13pin" terminalId="connector13terminal" layer="breadboard"/>
        </breadboardView>
        <schematicView>
          <p svgId="connector13pin" terminalId="connector13terminal" layer="schematic"/>
        </schematicView>
        <pcbView>
          <p svgId="connector13pin" layer="copper0"/>
          <p svgId="connector13pin" layer="copper1"/>
        </pcbView>
      </views>
    </connector>
  </connectors>
  <schematic-subparts>
    <subpart id="subpart0" label="">
      <connectors>
        <connector id="connector6"/>
        <connector id="connector13"/>
      </connectors>
    </subpart>
    <subpart id="subpart1" label="1">
      <connectors>
        <connector id="connector0"/>
        <connector id="connector1"/>
        <connector id="connector2"/>
      </connectors>
    </subpart>
    <subpart id="subpart2" label="2">
      <connectors>
        <connector id="connector3"/>
        <connector id="connector4"/>
        <connector id="connector5"/>
      </connectors>
    </subpart>
    <subpart id="subpart3" label="3">
      <connectors>
        <connector id="connector7"/>
        <connector id="connector8"/>
        <connector id="connector9"/>
      </connectors>
    </subpart>
    <subpart id="subpart4" label="4">
      <connectors>
        <connector id="connector10"/>
        <connector id="connector11"/>
        <connector id="connector12"/>
      </connectors>
    </subpart>
  </schematic-subparts>
</module>
