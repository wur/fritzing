import shutil
import re
import bs4
import os
from mdutils.mdutils import MdUtils
from genericpath import isfile
import os
import requests
import json
import time
import zipfile
import xmltodict
import xml.etree.ElementTree as ET
from mdutils.mdutils import MdUtils

category_pages = {}

def get_all_parts():
    files = []

    dirlist = ['./parts']

    while len(dirlist) > 0:
        for (dirpath, dirnames, filenames) in os.walk(dirlist.pop()):
            dirlist.extend(dirnames)
            files.extend(map(lambda n: os.path.join(*n), zip([dirpath] * len(filenames), filenames)))
    return files

def text_cleaner(text):
    text = re.sub("\s+", " ", text)
    text = re.sub(":", " ", text)
    text = re.sub(" +", " ", text)
    text = text.strip()

    return text

def write_category_page(fzp_folder, xml_dict):
    if not os.path.isdir("./docs/category"):
        os.mkdir("./docs/category/")
    
    category = fzp_folder.split("/")[1]
    if category not in category_pages:
        category_pages[category] = MdUtils(file_name="./docs/category/" + category + ".md", title='Parts page of category ' + category)
        category_pages[category].new_header(level=1, title='Parts page of category ' + category)
    
    mdFile = category_pages[category]
    module = xml_dict['module']

    if 'title' in module:
        title = module['title']
        if title == None: 
            title = "Part with no title"
    else:
        title = "Part with no title"

    mdFile.new_header(level=2, title=title)

    # Create a new page for each part / category
    os.makedirs("./docs/" + fzp_folder, exist_ok=True)   

    if 'author' in module:
        author = module['author']
        if author != None:
            # author = text_cleaner(author)
            mdFile.new_line("**Author:** " + author.strip())    
    if 'label' in module:
        label = module['label']
        if label != None:
            mdFile.new_line("**Label:** " + label.strip())
    if 'description' in module:
        if type(module['description']) == str:
            description = bs4.BeautifulSoup(module['description'], features="lxml").find('body').text
            if description != None and len(description.strip()) > 0:
                # description = text_cleaner(description)
                mdFile.new_line("**Description:** " + description.strip())
    if '@fritzingVersion' in module:
        mdFile.new_line("**fritzing version:** " + module['@fritzingVersion'].strip())
    if 'version' in module and module['version'] != None:
        if type(module['version']) == str:
            mdFile.new_line("**Version:** " + module['version'].strip())
        else:
            print("To be implemented ", module['version'])
    if 'date' in module:
        if type(module['date']) == str:
            mdFile.new_line("**Date:** " + module['date'].strip())
        else:
            print("To be implemented ", module['version'])
    


    # Copy SVG to parts docs folder
    for file in os.listdir(fzp_folder):
        # print(fzp_folder)
        if file.endswith(".svg"):
            svg_file = fzp_folder + "/" + file
            svg_file_destination = "docs/"  + svg_file
            shutil.copy(svg_file, svg_file_destination)
            # print(svg_file_destination)
            # Breadboard
            if file.startswith("svg.breadboard"):
                mdFile.new_line("**Breadboard:**")
                mdFile.new_line(mdFile.new_inline_image(text=file, path="../" +svg_file))
            # PCB
            if file.startswith("svg.pcb"):
                mdFile.new_line("**PCB:**")
                mdFile.new_line(mdFile.new_inline_image(text=file, path="../" +svg_file))
            # Schematic
            if file.startswith("svg.schematic"):
                mdFile.new_line("**Schematic:**")
                mdFile.new_line(mdFile.new_inline_image(text=file, path="../" +svg_file))
    
    # File link, zip the folder and place it here as fzpz
    list_files = os.listdir(fzp_folder)
    # print(list_files)
    zip_file = "./docs/" + fzp_folder + "/" + os.path.basename(fzp_folder)+ '.fzpz'
    # print(zip_file)
    with zipfile.ZipFile(zip_file, 'w') as zipF:
        for file in list_files:
            zipF.write(fzp_folder + "/" + file, file, compress_type=zipfile.ZIP_DEFLATED)
        mdFile.new_line("File link: " + mdFile.new_inline_link(link= "/../" + fzp_folder + "/" + os.path.basename(fzp_folder)+ '.fzpz', text="download", bold_italics_code='cbi'))

    # Write info about the part
    # print(xml_dict)
    # Write the file
    mdFile.new_line('---')
    mdFile.new_line('---')
    # mdFile.create_md_file()

def fzpz():
    if not os.path.isdir("docs"):
        os.mkdir("docs")

    mdFile = MdUtils(file_name='./docs/Parts_overview',title='Markdown File Example')
    mdFile.create_md_file()
    mdFile.new_header(level=1, title='Parts overview')
    
    # Process all the parts
    for category in os.listdir("parts"):
        mdFile.new_header(level=2, title='Category ' + category)
        for fzpz_file in os.listdir("parts/" + category):
            fzpz_file = "parts/" + category + "/" + fzpz_file
            if not os.path.isfile(fzpz_file) and not fzpz_file.endswith("fzpz"): continue
            # Folder place holder
            folder = fzpz_file.replace(".fzpz", "")
            # If not exists, unpack the fzpz file
            if fzpz_file.endswith('fzpz') and not os.path.isdir(folder): 
                file_path = fzpz_file
                with zipfile.ZipFile(file_path, 'r') as zip_ref:
                    zip_ref.extractall(folder)
        # Table header
        table_header = ["title", "description", "author"]
        table_content = []
        table_content.extend(table_header)
        for fzp_folder in os.listdir("parts/" + category):
            
            fzp_folder = "parts/" + category + "/" + fzp_folder
            if not os.path.isdir(fzp_folder): continue
            # Inspect each folder
            for fzp_file in os.listdir(fzp_folder):
                fzp_file = fzp_folder + "/" + fzp_file
                # Only the fzp files
                if not fzp_file.endswith(".fzp"): continue
                # Process the content
                with open(fzp_file, 'r', encoding='utf-8') as fzp_file:
                    row = ['xxx'] * len(table_header)
                    my_xml = fzp_file.read()
                    # Not all conversions go well
                    try:
                        xml_dict = xmltodict.parse(my_xml, encoding='utf-8', process_namespaces=False, namespace_separator=':')
                    except:
                        print("Conversion failed for", fzp_file)
                        continue
                    pretty = json.dumps(xml_dict, indent=4)
                    module = xml_dict['module']
                    # print(module.keys())
                    if 'author' in module:
                        # print(module['author'])
                        author = module['author']
                        if author != None:
                            author = text_cleaner(author)
                            # print(author)
                            row[table_content.index('author')] = author
                    # if 'label' in module:
                        # print("LABEL: ", module['label'])
                        # row[table_content.index('label')] = module['label']
                    if 'description' in module:
                        if type(module['description']) == str:
                            description = bs4.BeautifulSoup(module['description'], features="lxml").find('body').text
                            if len(description.strip()) > 0:
                                description = text_cleaner(description)
                                row[table_content.index('description')] = description
                    if 'title' in module:
                        title = module['title']
                        if title != None: 
                            title = text_cleaner(title)
                            # Add link to the document we create later
                            link = "/" + fzp_folder + "/part/"
                            # http://localhost:8000/parts/other/0.9v-5V-booster-usb-output_dLILHdPwDWdfg32h0eodi7qqp6H/part/
                            # http://localhost:8000/docs/parts/other/JFET_P_DGS_NTE489_yKFt4j3VvVTSCxx8prTCDjAb0ii/part.md
                            row[table_content.index('title')] = mdFile.new_inline_link(link, title)
                    #     print(module['title'])
                    # if 'url' in module:
                    #     print(module['url'])
                    # if 'tags' in module:
                    #     print(module['tags'])
                    # family = False
                    # if 'properties' not in module: continue
                    # if type(module['properties']['property']) != list:
                    #     module['properties']['property'] = [module['properties']['property']]
                    # for property in module['properties']['property']:
                    #     if property['@name'] == 'family':
                    #         family = True
                    #         if '#text' in property:
                    #             print(property['#text'])
                    # if not family:
                    #     print("other")
                    # for property in module['properties']['property']:
                        # print(property)
                    table_content.extend(row)
                    # print(len(table_content))
                    write_category_page(fzp_folder, xml_dict)
        mdFile.new_line()
        mdFile.new_table(columns=len(table_header), rows=int(len(table_content) / len(table_header)), text=table_content, text_align='center')

    mdFile.create_md_file()
    # Closing the categories
    for category in category_pages:
        category_pages[category].create_md_file()


if __name__ == '__main__':
    fzpz()